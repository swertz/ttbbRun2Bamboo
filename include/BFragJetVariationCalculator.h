#ifndef GENJETVAR
#define GENJETVAR

#include <utility>
#include <vector>
#include <string>

#include <ROOT/RVec.hxx>

namespace { // copied from https://gitlab.cern.ch/cp3-cms/CMSJMECalculators/-/blob/main/interface/JMESystematicsCalculators.h
class ModifiedPtMCollection { // map of variation collections
public:
  using compv_t = ROOT::VecOps::RVec<float>;

  ModifiedPtMCollection() = default;
  ModifiedPtMCollection(std::size_t n, const compv_t& pt, const compv_t& mass)
    : m_pt(n, pt), m_mass(n, mass) {}

  std::size_t size() const { return m_pt.size(); }

  const compv_t& pt(std::size_t i) const { return m_pt[i]; }
  const compv_t& mass(std::size_t i) const { return m_mass[i]; }

  void set(std::size_t i, const compv_t& pt, const compv_t& mass) {
    m_pt[i] = pt;
    m_mass[i] = mass;
  }
  void set(std::size_t i, compv_t&& pt, compv_t&& mass) {
    m_pt[i] = std::move(pt);
    m_mass[i] = std::move(mass);
  }
private:
  std::vector<compv_t> m_pt;
  std::vector<compv_t> m_mass;
};
}

class BFragJetVariationCalculator {
public:
    using p4compv_t = ROOT::VecOps::RVec<float>;
    using result_t = ::ModifiedPtMCollection;

    BFragJetVariationCalculator(float size): _variations({ {"nominal", 1.}, {"bFragdown", 1 - size}, {"bFragup", 1 + size} }) {}

    std::vector<std::string> available(const std::string& attr = {}) const {
        std::vector<std::string> res;
        for (const auto& it: _variations)
            res.push_back(it.first);
        return res;
    }

    result_t produce(const p4compv_t& jet_pt, const p4compv_t& jet_mass, const ROOT::VecOps::RVec<int>& jet_hadronFlavour) const {
        const auto nJets = jet_pt.size();
        result_t out{_variations.size(), jet_pt, jet_mass};
        for (std::size_t iVar = 0; iVar < _variations.size(); iVar++) {
            p4compv_t var_pt{jet_pt}, var_mass{jet_mass};
            for (std::size_t iJet = 0; iJet < nJets; iJet++) {
                if (jet_hadronFlavour[iJet] != 5)
                    continue;
                var_pt[iJet] *= _variations[iVar].second;
                var_mass[iJet] *= _variations[iVar].second;
            }
            out.set(iVar, std::move(var_pt), std::move(var_mass));
        }
        return out;
    }



private:
    std::vector<std::pair<std::string, float>> _variations;
};

#endif
