#!/usr/bin/env python

import argparse
import yaml
import os

import HistogramTools as HT

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', nargs='+', help='input file(s)')
parser.add_argument('-c', '--config', help='yml file for cross sections')
parser.add_argument('-o', '--output', help='output file name (only works if single input file)')
parser.add_argument('-f', '--folder', help='output folder')

args = parser.parse_args()

import ROOT as R
R.gROOT.SetBatch(True)

for inPath in args.input:

    inFile = HT.openFileAndGet(inPath)
    hists = dict()
    HT.readRecursiveDirContent(hists, inFile, resetDir=False)

    runsTree = hists['Runs']
    sumW = sum([entry.genEventSumw for entry in runsTree])

    with open(args.config) as _f:
        ymlCfg = yaml.load(_f)
    xs = ymlCfg['files'][os.path.basename(inPath)]['cross-section']
    lumi = ymlCfg['configuration']['luminosity']

    def apply(myDict, func, inherit):
        for key, obj in myDict.items():
            if isinstance(obj, dict):
                apply(obj, func, inherit)
            elif obj.InheritsFrom(inherit):
                func(obj)

    def normalise(hist):
        hist.Scale(lumi * xs / sumW)
    apply(hists, normalise, "TH1")

    if args.output and len(args.input) == 1:
        outPath = args.output
    else:
        outPath = os.path.join(args.folder, os.path.basename(inPath))
    print(f"Writing normalised histograms to {outPath}")

    outFile = HT.openFileAndGet(outPath, "recreate")
    HT.writeRecursiveDirContent(hists, outFile)
    outFile.Close()

    inFile.Close()

print("Done")
