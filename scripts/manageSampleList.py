#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse
import yaml
import subprocess
import json
import re

from utils import fillSampleTemplate

campaign_dict = {
    "RunIIULNanoAODv8": {
        "data": {
            "2016ULpreVFP": "UL2016_MiniAODv1_NanoAODv2",
            "2016ULpostVFP": "UL2016_MiniAODv1_NanoAODv2", # "HIPM" not consistent here...
            "2017UL": "UL2017_MiniAODv1_NanoAODv2",
            "2018UL": "UL2018_MiniAODv1_NanoAODv2",
        },
        "mc": {
            "2016ULpreVFP": "RunIISummer20UL16NanoAODAPVv2-106X_mcRun2_asymptotic_preVFP_v9",
            "2016ULpostVFP": "RunIISummer20UL16NanoAODv2-106X_mcRun2_asymptotic_v15",
            "2017UL": "RunIISummer20UL17NanoAODv2-106X_mc2017_realistic_v8",
            "2018UL": "RunIISummer20UL18NanoAODv2-106X_upgrade2018_realistic_v15_L1v1",
        }
    },
    "RunIIULNanoAODv9": {
        "data": {
            "2016ULpreVFP": "HIPM_UL2016_MiniAODv2_NanoAODv9",
            "2016ULpostVFP": "-UL2016_MiniAODv2_NanoAODv9", # '-' prefix to avoid confusion with preVFP
            "2017UL": "UL2017_MiniAODv2_NanoAODv9",
            "2018UL": "UL2018_MiniAODv2_NanoAODv9",
        },
        "mc": {
            "2016ULpreVFP": "RunIISummer20UL16NanoAODAPVv9-106X_mcRun2_asymptotic_preVFP_v11",
            "2016ULpostVFP": "RunIISummer20UL16NanoAODv9-106X_mcRun2_asymptotic_v17",
            "2017UL": "RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9",
            "2018UL": "RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1",
        }
    },
}

class SampleError:
    def __init__(self, sample, instead=None):
        self.sample = no_prfx(sample)
        self.instead = instead
class SampleNotFoundError(SampleError):
    msg = "The following samples could not be found in DAS:"
class SampleVersionError(SampleError):
    msg = "The following samples have a wrong version ('-vX'):"
class WrongProcessName(SampleError):
    msg = "The following samples have inconsistent process names across eras:"
class WrongCampaignString(SampleError):
    msg = "The following samples have unexpected campaign strings:"
class WrongTier(SampleError):
    msg = "The following samples have unexpected data tiers:"
class ProductionWarning(SampleError):
    msg = "The following samples exist but are not in 'VALID' state:"

class ErrorCollector:
    def __init__(self):
        self.errors = {}
    def add(self, error):
        self.errors.setdefault(type(error), []).append(error)
    def report(self):
        for typ,errors in self.errors.items():
            if len(errors):
                print(typ.msg)
                print("".join(["#"]*len(typ.msg)))
                print()
                for err in errors:
                    print(f"{err.sample:<80}" + (f" --> should be {err.instead}" if err.instead else ""))
                print()
errorCollector = ErrorCollector()

def version_from_campaign(campaign):
    """From campaign string "RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-vX", return X"""
    result = re.search(r'.*v([0-9])$', campaign)
    return int(result.group(1))

def no_prfx(smp):
    if ":" in smp:
        return smp.split(":")[1]
    else:
        return smp

def split_sample_name(sample):
    """From das:/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v1/NANOAODSIM, return:
        ("TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8",
        "RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v1",
        "NANOAODSIM")
    """
    sample = no_prfx(sample)
    process,campaign,tier = sample.split("/")[1:]
    return process,campaign,tier

def das_query(query):
    result = subprocess.check_output(["dasgoclient", "-json", "-query", query])
    return json.loads(result)

def check_sample_exists(sample):
    """return Bool,Str: (dataset exists, dataset status) where status can be VALID, PRODUCTION, INVALID"""
    sample = no_prfx(sample)
    das_result = das_query(f'dataset dataset={sample}')
    if len(das_result) == 0:
        return False,None
    for res in das_result:
        if "dbs3:dataset_info" in res["das"]["services"]:
            status = res["dataset"][0]["dataset_access_type"]
            if status == "INVALID":
                return False,status # invalid considered as non-existent
            return True,status

def find_sample_version(sample):
    process,campaign,tier = split_sample_name(sample)
    old_version = version_from_campaign(campaign)
    for i in range(1, 6):
        new_campaign = campaign[:-2] + f"v{i}"
        new_sample = "/".join(("", process, new_campaign, tier))
        exists,status = check_sample_exists(new_sample)
        if exists:
            errorCollector.add(SampleVersionError(sample, f"v{i}"))
            if status != "VALID":
                errorCollector.add(ProductionWarning(new_sample + f" ({status})"))
            return
    errorCollector.add(SampleNotFoundError(sample))

def check_sample(sample, era, version, is_data, find_version=False):
    exists,status = check_sample_exists(sample)
    if not exists:
        if find_version:
            find_sample_version(sample)
        else:
            errorCollector.add(SampleNotFoundError(sample))
    elif status != "VALID":
        errorCollector.add(ProductionWarning(sample + f" ({status})"))

    process,campaign,tier = split_sample_name(sample)

    campaign_string = campaign_dict[version][("data" if is_data else "mc")][era]
    if campaign_string not in campaign:
        errorCollector.add(WrongCampaignString(sample, instead=campaign_string))

    tier_ok = True
    if tier == "NANOAOD":
        if not is_data:
            errorCollector.add(WrongTier(sample, instead="NANOAODSIM"))
    elif tier == "NANOAODSIM":
        if is_data:
            errorCollector.add(WrongTier(sample, instead="NANOAOD"))
    else:
        errorCollector.add(WrongTier(sample, instead=("NANOAOD" if is_data else "NANOAODSIM")))

def check_sample_template(template, version, eras=None, find_version=False):
    for smpNm,smpCfg in template.items():
        if eras and smpCfg["era"] not in eras:
            continue
        if "db" in smpCfg:
            check_sample(smpCfg["db"], smpCfg["era"], version, smpCfg.get("group", "") == "data", find_version)
        elif "dbs" in smpCfg:
            for era,smp in smpCfg["dbs"].items():
                check_sample(smp, era, version, smpCfg.get("group", "") == "data", find_version)
            processes = (split_sample_name(smp)[0] for smp in smpCfg["dbs"].values())
            if not len(set(processes)) == 1:
                errorCollector.add(WrongProcessName(smpNm))

def change_version(v_from, v_to):
    sed_string = []
    for typ in "mc", "data":
        for era,cmp_from in campaign_dict[v_from][typ].items():
            cmp_to = campaign_dict[v_to][typ][era]
            sed_string.append(f"s|{cmp_from}|{cmp_to}|g\n")
    print("\n".join(sed_string))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Tool to validate sample template and help moving from one nanoAOD version to the next')
    parser.add_argument('--samples', help='Sample template YAML file')
    parser.add_argument('--eras', type=list, help='Filter eras in sample list')
    parser.add_argument('--version', default='RunIIULNanoAODv9', help='Processing version string')
    parser.add_argument('--check', action='store_true', help='Check that all samples have consistent process names, campaign name, and actually exist in DAS')
    parser.add_argument('--find-version', action='store_true', help="If sample not found in DAS, check if it's because the version ('-vX') is wrong")
    parser.add_argument('--change-version-to', help="Output sed script to obtain new config with updated processing version strings (e.g. RunIIULNanoAODv8 -> RunIIULNanoAODv9). Then run as 'sed -f (output) path_to_YAML_file'.")
    args = parser.parse_args()

    with open(args.samples) as fh:
        sample_template = yaml.safe_load(fh)

    if args.check:
        print("Checking samples... this might take a while!\n")
        check_sample_template(sample_template, args.version, args.eras, args.find_version)
        errorCollector.report()

    if args.change_version_to:
        change_version(args.version, args.change_version_to)
