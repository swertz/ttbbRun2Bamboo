#!/usr/bin/env python

import argparse
import os,sys
import yaml

import HistogramTools as HT

import ROOT as R
R.gROOT.SetBatch(True)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(help="Plot all 2D slices of a TH3 in a given direction")
    parser.add_argument('-i', '--input', help='input ROOT file')
    parser.add_argument('-n', '--name', help='histogram name')
    parser.add_argument('-o', '--output', help='output folder')
    parser.add_argument('-a', '--axes', nargs='+', default=['x', 'y', 'z'], choices=['x', 'y', 'z'], help='Axes to create slices from')

    args = parser.parse_args()

    hist = HT.loadHisto(args.input, args.name)

    if not hist.InheritsFrom("TH3"):
        raise Exception(f"Histogram {args.hist} is not 3D!")

    if not os.path.isdir(args.output):
        os.makedirs(args.output, exist_ok=True)

    for axis in args.axes:
        HT.plot2DSlices(hist, args.output, axis)
