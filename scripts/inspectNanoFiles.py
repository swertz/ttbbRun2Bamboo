#!/usr/bin/env python

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)

import argparse
import yaml
import os
import csv

from utils import fillSampleTemplate
from bamboo.analysisutils import sample_resolveFiles, readEnvConfig

def analyzeSample(name, sample):
    path = sample["files"][0]
    tf = ROOT.TFile.Open(path)
    tt = tf.Get("Events")
    tt.GetEntry(0)

    issue = False
    msg = f"==== {name} ====\n"

    isTopNano = hasattr(tt, "GenJet_nBHadFromT")
    if isTopNano:
        msg += "- Is topNanoAOD\n"

    # LHE scale weights
    expNScaleWeights = 9
    if tt.nLHEScaleWeight != expNScaleWeights:
        issue = True
        msg += f"- nLHEScaleWeights = {tt.nLHEScaleWeight} instead of {expNScaleWeights} -- weights for first event are {[i for i in tt.LHEScaleWeight]}\n"
    # expScaleWeightDocStr = "LHE scale variation weights (w_var / w_nominal); [0] is renscfact=0.5d0 facscfact=0.5d0 ; [1] is renscfact=0.5d0 facscfact=1d0 ; [2] is renscfact=0.5d0 facscfact=2d0 ; [3] is renscfact=1d0 facscfact=0.5d0 ; [4] is renscfact=1d0 facscfact=1d0 ; [5] is renscfact=1d0 facscfact=2d0 ; [6] is renscfact=2d0 facscfact=0.5d0 ; [7] is renscfact=2d0 facscfact=1d0 ; [8] is renscfact=2d0 facscfact=2d0 "
    if tt.nLHEScaleWeight > 4 and abs(tt.LHEScaleWeight[4] - 1) > 1e-4:
        issue = True
        msg += f"- LHEScaleWeight[4] for first event is too different from 1: {tt.LHEScaleWeight[4]}\n"
    msg += "- LHE scale weights title: "
    msg += f"'{tt.GetBranch('nLHEScaleWeight').GetTitle()}'\n"

    # PS weights
    if tt.nPSWeight == 46:
        msg += "- Has all 46 PS weights\n"
    elif tt.nPSWeight == 14:
        msg += "- Has 2+12 def/con/red PS weights\n"
    elif tt.nPSWeight == 4:
        msg += "- Has regular 4 PS weights\n"
    if isTopNano:
        if tt.nPSWeight != 46 and tt.nPSWeight != 14:
            issue = True
            msg += f"- nPSWeight = {tt.nPSWeight}, for topNanoAOD expected either 14 or 46\n"
    else:
        if tt.nPSWeight != 4:
            issue = True
            msg += f"- nPSWeight = {tt.nPSWeight} instead of 4\n"

    # PDF weights
    msg += f"- Has tt.nLHEPdfWeights = {tt.nLHEPdfWeight}"
    if tt.nLHEPdfWeight != 101 and tt.nLHEPdfWeight != 103:
        issue = True
        msg += f"- nLHEPdfWeights = {tt.nLHEPdfWeight} instead of 101 or 103 -- weights for first event are {[i for i in tt.LHEPdfWeight]}\n"
    if tt.nLHEPdfWeight > 0 and abs(tt.LHEPdfWeight[0] - 1) > 1e-4:
        issue = True
        msg += f"- LHEPdfWeight[0] for first event is too different from 1: {tt.LHEPdfWeight[0]}\n"
    msg += "- LHE PDF weights title: "
    msg += f"'{tt.GetBranch('nLHEPdfWeight').GetTitle()}'\n"

    msg += "\n"

    return issue,msg

def countEvents(smpCfg):
    chain = ROOT.TChain("Events")
    for file_ in smpCfg["files"]:
        chain.Add(file_)
    return chain.GetEntries()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Check the MC samples have all the expected info (event weights etc.)')
    parser.add_argument("-s", "--samples", nargs='*', required=True, help="Sample template YML file")
    parser.add_argument("-a", "--analysis", required=True, help="Analysis YML file")
    parser.add_argument('-e', '--eras', nargs='*', help='Restrict to given era(s)')
    parser.add_argument('-c', '--count', help='Count number of events, put in csv file')
    options = parser.parse_args()

    noIssue,withIssue = dict(),dict()

    with open(options.analysis) as file_:
        anaCfg = yaml.load(file_, Loader=yaml.SafeLoader)
    dbCache = anaCfg["dbcache"]
    cfgDir = os.path.dirname(options.analysis)

    envConfig = readEnvConfig()

    samples = {}

    for path in options.samples:
        with open(path) as _f:
            smps = yaml.load(_f, Loader=yaml.SafeLoader)
            samples.update(fillSampleTemplate(smps, options.eras, False))
    for smpNm,smpCfg in samples.items():
        smpCfg["files"] = sample_resolveFiles(smpNm, smpCfg, dbCache, envConfig, cfgDir)

    for smpNm,smpCfg in samples.items():
        # skipping data since we're only checking the event weights (for now...)
        if smpCfg.get("group", "") == "data":
            continue
        issue,msg = analyzeSample(smpNm, smpCfg)
        era = smpCfg["era"]
        if issue:
            withIssue.setdefault(era, "")
            withIssue[era] += msg
        else:
            noIssue.setdefault(era, "")
            noIssue[era] += msg

    print("Samples with issues:\n")
    for era in sorted(withIssue.keys()):
        print(f"== ERA: {era} ==\n")
        print(withIssue[era])

    print("Samples without issues:\n")
    for era in sorted(noIssue.keys()):
        print(f"== ERA: {era} ==\n")
        print(noIssue[era])

    print("\nCounting events...\n")
    if options.count:
        with open(options.count, "w") as file_:
            writer = csv.writer(file_)
            writer.writerow(["sample", "era", "is_data", "n_events"])
            for smpNm,smpCfg in samples.items():
                nEvts = countEvents(smpCfg)
                print(f"{smpNm}: {nEvts} events")
                writer.writerow([smpNm, smpCfg["era"], smpCfg.get("group", "") == "data", nEvts])
