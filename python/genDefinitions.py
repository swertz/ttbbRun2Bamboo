from bamboo import treefunctions as op
from bamboo.plots import EquidistantBinning as EqBin
from bamboo.plots import Plot
import utils

#### Object definitions

def muonDef(mu):
    return op.AND(
        mu.pt > 26., op.abs(mu.eta) < 2.4,
        op.abs(mu.pdgId) == 13,
    )

def vetoMuonDef(mu):
    return op.AND(
        mu.pt > 15., op.abs(mu.eta) < 2.4,
        op.abs(mu.pdgId) == 13,
    )

def eleDef(ele):
    return op.AND(
        op.AND(ele.pt > 29, op.abs(ele.eta) < 2.5),
        op.abs(ele.pdgId) == 11,
    )

def vetoEleDef(ele):
    return op.AND(
        ele.pt > 15., op.abs(ele.eta) < 2.5,
        op.abs(ele.pdgId) == 11,
    )

def jetDef(jet):
    return op.AND(
        jet.pt > 25., op.abs(jet.eta) < 2.4
    )

def cleanJets(jets, muons, electrons):
    return op.select(jets, lambda jet: op.AND(
            op.NOT(op.rng_any(electrons, lambda ele: op.deltaR(jet.p4, ele.p4) < 0.4 )),
            op.NOT(op.rng_any(muons, lambda mu: op.deltaR(jet.p4, mu.p4) < 0.4 ))
        ))

def buildLeptonSelection(noSel, muons, vetoMuons, electrons, vetoElectrons, tag="", prefix=''):
    muonChannel = op.AND(
            op.rng_len(muons) == 1,
            op.rng_len(vetoMuons) == 1,
            op.rng_len(vetoElectrons) == 0
        )
    electronChannel = op.AND(
            op.rng_len(vetoMuons) == 0,
            op.rng_len(vetoElectrons) == 1,
            op.rng_len(electrons) == 1
        )
    # the 2 channels are exclusive, so we can safely combine them in a single selection
    return noSel.refine(f"{prefix}lepton{tag}", cut=op.OR(muonChannel, electronChannel))

def makeFineJetPlots(sel, jets, uname, maxJet=4, bJets=False):
    plots = []
    
    fine_tag = "fineBinning"
    if bJets:
        b_tag = "b"
    else:
        b_tag = ""
        
    jet_pt_ubound = [ 530., 430., 330., 230., 180., 130., 130., 130., 130., 130., ]
    jet_pt_bins = [ EqBin(3000, 20., jet_pt_ubound[jet_i])     for jet_i in range(10)  ]
    jet_eta_bins = [ EqBin(1250, 0, 2.4 )  for jet_i in range(10)   ]
    jet_HT_bins   = EqBin( 5000, 0., 1000.) 
        
    for i in range(maxJet):
        plots.append(Plot.make1D(f"{fine_tag}_{uname}_{b_tag}jet{i+1}_pt", jets[i].pt, sel,
                jet_pt_bins[i], title=f"Fine Bin Plot {utils.getCounter(i+1)} jet p_{{T}} (GeV)",
                plotopts=utils.getOpts(uname)))
        plots.append(Plot.make1D(f"{fine_tag}_{uname}_{b_tag}jet{i+1}_eta", op.abs(jets[i].eta), sel,
                jet_eta_bins[i], title=f"Fine Bin Plot {utils.getCounter(i+1)} jet eta",
                plotopts=utils.getOpts(uname, **{"log-y": False})))

    #HT
    plots.append(Plot.make1D(f"{fine_tag}_{uname}_{b_tag}jet_HT", op.rng_sum(jets, lambda j: j.pt), sel,
            jet_HT_bins, title=f"Fine Bin Plot jet HT",
            plotopts=utils.getOpts(uname, **{"log-y": False})))
    return plots
    
def makeFineExtraJetPlots(sel, jets, uname):
    plots = []
    n_extra_jet = 2

    fine_tag = "fineBinning"
    extra_jet_pt_bins = [ EqBin( 2000, 20., 630. - (jet_i) * 300. )     for jet_i in range(n_extra_jet) ]
    extra_jet_eta_bins = [ EqBin(1000, 0, 2.4) for jet_i in range(n_extra_jet) ]
    extra_jet_dr_bins = EqBin(2000, 0.4, 5.74 )  
    extra_jet_mass_bins = EqBin( 2000, 0., 400.)
    extra_jet_pt_tot_bins = EqBin( 2000, 0., 600.) 
        
    for i in range(2):
        plots.append(Plot.make1D(f"{fine_tag}_{uname}_extra_jet{i+1}_pt", jets[i].pt, sel,
                extra_jet_pt_bins[i], title=f"Fine Bin Plot {utils.getCounter(i+1)} extra jet p_{{T}} (GeV) (6j4b)",
                plotopts=utils.getOpts(uname)))
        plots.append(Plot.make1D(f"{fine_tag}_{uname}_extra_jet{i+1}_eta", op.abs(jets[i].eta), sel,
                extra_jet_eta_bins[i], title=f"Fine Bin Plot {utils.getCounter(i+1)} extra jet eta (6j4b)",
                plotopts=utils.getOpts(uname)))
    plots.append(Plot.make1D(f"{fine_tag}_{uname}_extra_jet_DR", op.deltaR(jets[0].p4, jets[1].p4),
            sel, extra_jet_dr_bins , title="Fine Bin Plot Extra jets DR (6j4b)",
            plotopts=utils.getOpts(uname)))
    plots.append(Plot.make1D(f"{fine_tag}_{uname}_extra_jet_M", op.invariant_mass(jets[0].p4, jets[1].p4),
            sel, extra_jet_mass_bins,
            title="Fine Bin Plot Extra jets invariant mass (GeV)", plotopts=utils.getOpts(uname)))
    plots.append(Plot.make1D(f"{fine_tag}_{uname}_extra_jet_pt", (jets[0].p4 + jets[1].p4).Pt(),
            sel, extra_jet_pt_tot_bins, title="Fine Bin Plot Extra jet pair p_{T} (GeV) (6j4b)",
            plotopts=utils.getOpts(uname)))
    
    return plots

def makeFineMaxMbbPairPlots(sel, maxMbbPair, uname):
    plots = []
    fine_tag = "fineBinning"
        
    plots.append(Plot.make1D(f"{fine_tag}_{uname}_largest_Mbb", 
        op.invariant_mass(maxMbbPair[0].p4, maxMbbPair[1].p4),
        sel, EqBin(2500, 0., 1000.), title="Fine Bin Plot Largest Mbb (6j4b)", plotopts=utils.getOpts(uname)))

    return plots

def makeFineExtraLightJetPlots(sel, lightJets_notW, softest_bJet, uname):
    plots = []
    fine_tag = "fineBinning"
    jet_phi_bins   = EqBin( 1000, 0., 3.1416 ) 
    
    plots.append(Plot.make1D(f"{fine_tag}_{uname}_extraLightJet_pt", lightJets_notW[0].pt, sel,
            EqBin(600, 20., 520.), title="Add. light jet p_{T} (GeV)", plotopts=utils.getOpts(uname)))

    plots.append(Plot.make1D(f"{fine_tag}_{uname}_dPhi_lj_b4", op.abs(lightJets_notW[0].p4.Phi() - softest_bJet.p4.Phi()), sel,
            jet_phi_bins, title=f"Fine Bin Plot delta Phi(extra light jet, softest b jet)",
            plotopts=utils.getOpts(uname, **{"log-y": False})))
    return plots
    
    
def makeFineAvgDRPlots(sel, genJetPairs, uname):
    plots = []
    fine_tag = "fineBinning"

    plots.append(Plot.make1D(f"{fine_tag}_{uname}_average_DRbb", op.rng_mean(op.map(genJetPairs, lambda p: op.deltaR(p[0].p4, p[1].p4))), 
        sel, EqBin(600, 0.6, 4.), title="Average #Delta R(bb)", plotopts=utils.getOpts("uname")))
    return plots
    
    
def makeFineLightJetPlots(sel, jets, uname):
    plots = []
    fine_tag = "fineBinning"
    
    jet_HT_bins   = EqBin( 2500, 0., 500.) 

    plots.append(Plot.make1D(f"{fine_tag}_{uname}_LightJet_HT", op.rng_sum(jets, lambda j: j.pt), sel,
            jet_HT_bins, title=f"Fine Bin Plot light jet HT",
            plotopts=utils.getOpts(uname, **{"log-y": False})))
    return plots
