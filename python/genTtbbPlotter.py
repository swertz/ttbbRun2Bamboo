import os

import logging
logger = logging.getLogger("genTtbb plotter")

from bamboo.analysismodules import NanoAODHistoModule, NanoAODSkimmerModule

from bamboo.analysisutils import configureJets, makePileupWeight

from bamboo.plots import Plot
from bamboo.plots import EquidistantBinning as EqBin
from bamboo import treefunctions as op

import definitions as defs
import utils
import controlPlotDefinition as cp
import genDefinitions as genDefs
from genBaseTtbbPlotter import genBaseTtbbPlotter

class genTtbbPlotter(genBaseTtbbPlotter):
    """"""
    def __init__(self, args):
        super().__init__(args)

    def customizeAnalysisCfg(self, analysisCfg):
        super().customizeAnalysisCfg(analysisCfg)

        samples = analysisCfg["samples"]
        for smpNm in list(samples.keys()):
            #remove everything non-ttB
            if samples[smpNm].get("subprocess", "") != "ttB":
                samples.pop(smpNm)
            elif "SemiLeptonic" not in smpNm:
                samples.pop(smpNm)
            else:
                smpCfg = samples[smpNm]
                smpCfg["type"] = "signal"
                if "4f" in smpNm:
                    smpCfg["group"] = "ttbb_4fs"
                else:
                    smpCfg["group"] = "tt_5fs"

    def definePlots(s, t, noSel, sample=None, sampleCfg=None):
        super().defineObjects(t, noSel, sample, sampleCfg)
        era = sampleCfg["era"]
        plots = []

        ##### Muon and electron selection (exclusive!), merged
        genOneLepSel = genBaseTtbbPlotter.defineBaseSelections(s, t, noSel, sample, sampleCfg)

        # number of jets
        genOneLep5JetSel = genOneLepSel.refine("lepton_5jets", cut=op.rng_len(s.genCleanedJets) >= 5)
        genOneLep6JetSel = genOneLepSel.refine("lepton_6jets", cut=op.rng_len(s.genCleanedJets) >= 6)
        genOneLep7JetSel = genOneLepSel.refine("lepton_7jets", cut=op.rng_len(s.genCleanedJets) >= 7)
        # b tags
        genOneLep5Jet3BSel = genOneLep5JetSel.refine("lepton_5jets_3b", cut=op.rng_len(s.genBJets) >= 3)
        genOneLep6Jet3B3LSel = genOneLep6JetSel.refine("lepton_6jets_3b_3l", cut=op.AND(op.rng_len(s.genBJets) >= 3, op.rng_len(s.genLightJets) >= 3))
        genOneLep6Jet4BSel = genOneLep6JetSel.refine("lepton_6jets_4b", cut=op.rng_len(s.genBJets) >= 4)
        genOneLep7Jet4B3LSel = genOneLep7JetSel.refine("lepton_7jets_4b_3l", cut=op.AND(op.rng_len(s.genBJets) >= 4, op.rng_len(s.genLightJets) >= 3))

        # closest b-jet pair
        genBJetPairs = op.combine(s.genBJets, N=2)
        genMinDRbbPair = op.rng_min_element_by(genBJetPairs, lambda pair: op.deltaR(pair[0].p4, pair[1].p4))
        softest_bJet = op.rng_min_element_by(s.genBJets, lambda jet : jet.pt)

        # Largest Mbb pair
        genMaxMbbPair = op.rng_max_element_by(genBJetPairs, lambda pair: op.invariant_mass(pair[0].p4, pair[1].p4))

        # Extra light jets -> reconstruct hadronic W
        matchingChi2 = lambda w: op.pow((op.invariant_mass(w) - 79.6) / 4.4, 2.)
        lightJetPairs = op.combine(s.genLightJets, N=2)
        wCand = op.rng_min_element_by(lightJetPairs, lambda pair: matchingChi2(pair[0].p4 + pair[1].p4))
        lightJets_notW = op.select(s.genLightJets, lambda j: op.NOT(op.OR(j == wCand[0], j == wCand[1])))
        
        plots += genDefs.makeFineJetPlots( genOneLep6Jet4BSel, s.genCleanedJets, "1lep_6j_4b", maxJet=0 )
        
        plots += genDefs.makeFineExtraJetPlots( genOneLep6Jet4BSel, genMinDRbbPair, "1lep_6j_4b")
        plots += genDefs.makeFineMaxMbbPairPlots(genOneLep6Jet4BSel, genMaxMbbPair, "1lep_6j_4b" )
        plots += genDefs.makeFineAvgDRPlots(genOneLep6Jet4BSel, genBJetPairs, "1lep_6j_4b" )
        
        plots += genDefs.makeFineExtraLightJetPlots(genOneLep7Jet4B3LSel, lightJets_notW, softest_bJet, "1lep_7j_4b")
        plots += genDefs.makeFineExtraLightJetPlots(genOneLep6Jet3B3LSel, lightJets_notW, softest_bJet, "1lep_6j_3b")
        
        plots += genDefs.makeFineJetPlots( genOneLep6Jet4BSel, s.genBJets, "1lep_6j_4b", maxJet=4, bJets = True )
        plots += genDefs.makeFineLightJetPlots( genOneLep7Jet4B3LSel, s.genLightJets, "1lep_7j_4b" )
        plots += genDefs.makeFineLightJetPlots( genOneLep6Jet4BSel, s.genLightJets, "1lep_6j_4b" )
        plots += genDefs.makeFineJetPlots( genOneLep5Jet3BSel, s.genBJets, "1lep_5j_3b", maxJet=3, bJets = True )
        plots += genDefs.makeFineLightJetPlots( genOneLep6Jet3B3LSel, s.genLightJets, "1lep_6j_3b" )
        plots += genDefs.makeFineLightJetPlots( genOneLep5Jet3BSel, s.genLightJets, "1lep_5j_3b" )

        return plots

    def postProcess(self, taskList, config=None, workdir=None, resultsdir=None):
        super().postProcess(taskList, config, workdir, resultsdir, removeSignalOverlap=False)
        
        # run plotIt to have the 4f/5f comparison
        self.runPlotIt(taskList, config, workdir, resultsdir)
