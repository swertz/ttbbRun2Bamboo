import os
from functools import reduce

import logging
logger = logging.getLogger("genTtbb plotter")

from bamboo.analysismodules import NanoAODHistoModule, NanoAODSkimmerModule

from bamboo.analysisutils import configureJets, makePileupWeight

from bamboo.plots import Plot,SummedPlot
from bamboo.plots import EquidistantBinning as EqBin
from bamboo import treefunctions as op

import definitions as defs
import utils
import controlPlotDefinition as cp
import genDefinitions as genDefs

GEN_FLAGS = {
    "IsPrompt": 0,
    "IsDecayedLeptonHadron": 1,
    "IsTauDecayProduct": 2,
    "IsPromptTauDecayProduct": 3,
    "IsDirectTauDecayProduct": 4,
    "IsDirectPromptTauDecayProduct": 5,
    "IsDirectHadronDecayProduct": 6,
    "IsHardProcess": 7,
    "FromHardProcess": 8,
    "IsHardProcessTauDecayProduct": 9,
    "IsDirectHardProcessTauDecayProduct": 10,
    "FromHardProcessBeforeFSR": 11,
    "IsFirstCopy": 12,
    "IsLastCopy": 13,
    "IsLastCopyBeforeFSR": 14,
}

class genTtbbPlotter(NanoAODHistoModule):
    """"""
    def __init__(self, args):
        super(genTtbbPlotter, self).__init__(args)

        self.plotDefaults = {
            "y-axis"           : "Events",
            "log-y"            : "both",
            "y-axis-show-zero" : True,
            "save-extensions"  : ["pdf"],
            "show-ratio"       : True,
            "sort-by-yields"   : True,
            "normalized": True,
            "show-overflow": False,
        }

        self.doSysts = self.args.systematic

    def addArgs(self, parser):
        super(genTtbbPlotter, self).addArgs(parser)
        parser.add_argument("-s", "--systematic", action="store_true", help="Produce systematic variations")

    def customizeAnalysisCfg(self, analysisCfg):
        for smp in list(analysisCfg["samples"]):
            if not analysisCfg["samples"][smp].get("is_signal", False):
                del analysisCfg["samples"][smp]
            analysisCfg["samples"][smp]["type"] = "signal"

    def prepareTree(self, tree, sample=None, sampleCfg=None):
        if not self.isMC(sample):
            raise Exception("This plotter is only meant to be run on simulation!")

        tree,noSel,be,lumiArgs = NanoAODHistoModule.prepareTree(self, tree, sample=sample, sampleCfg=sampleCfg, description=defs.getNanoAODDescription(sampleCfg["era"], True), lazyBackend=True)

        # mandatory!!
        mcWgts = [ tree.genWeight ]
        noSel = noSel.refine("mcWeight", weight=mcWgts, autoSyst=self.doSysts)

        if self.doSysts:
            noSel = utils.addTheorySystematics(self, sample, sampleCfg, tree, noSel)

        return tree,noSel,be,lumiArgs

    # def defineSkimSelection(self, t, noSel, sample=None, sampleCfg=None):
    def definePlots(self, t, noSel, sample=None, sampleCfg=None):
        era = sampleCfg["era"]
        plots = []
        def mkPlt(*args, **kwargs):
            plots.append(Plot.make1D(*args, **kwargs))
        self.selId = 0
        def mkSel(sel, cut, wgt=None):
            self.selId += 1
            return sel.refine(sel.name + str(self.selId), cut, wgt)

        # hard process, last copy, not initial state
        def goodFlag(p):
            return op.AND(p.statusFlags & 2**GEN_FLAGS["IsLastCopy"], p.statusFlags & 2**GEN_FLAGS["FromHardProcess"], p.parent.idx >= 0)

        allGenParticles = t.GenPart
        genParticles = op.select(allGenParticles, goodFlag)

        # have a top but no W parent
        b_from_t = op.select(genParticles, lambda b: op.AND(op.abs(b.pdgId) == 5, op.AND(op.rng_any(b.ancestors, lambda p: op.abs(p.pdgId) == 6), op.NOT(op.rng_any(b.ancestors, lambda p: op.abs(p.pdgId) == 24)))))
        # have no top or W parent
        b_extra = op.select(genParticles, lambda b: op.AND(op.abs(b.pdgId) == 5, op.NOT(op.rng_any(b.ancestors, lambda p: op.OR(op.abs(p.pdgId) == 6, op.abs(p.pdgId) == 24)))))

        # have a top and a W parent
        genLightQuark = op.select(genParticles, lambda q: op.AND(op.abs(q.pdgId) >= 1, op.abs(q.pdgId) <= 4))
        light_from_w_from_t = op.select(genLightQuark, lambda q: op.AND(op.rng_any(q.ancestors, lambda p: op.abs(p.pdgId) == 24), op.rng_any(q.ancestors, lambda p: op.abs(p.pdgId) == 6)))

        mkPlt("incl_has_2b_2l", op.AND(op.rng_len(b_from_t) == 2, op.rng_len(light_from_w_from_t) == 2), noSel, EqBin(2, 0, 2))
        mkPlt("incl_n_b_from_t", op.rng_len(b_from_t), noSel, EqBin(5, 0, 5))
        mkPlt("incl_n_b_extra", op.rng_len(b_extra), noSel, EqBin(5, 0, 5))
        mkPlt("incl_n_light_from_wp_from_t", op.rng_len(light_from_w_from_t), noSel, EqBin(5, 0, 5))

        # exactly 2 b quarks from tops and 2 light quarks from t->W
        partonSel = noSel.refine("parton", op.AND(op.rng_len(b_from_t) == 2, op.rng_len(light_from_w_from_t) == 2))
        
        plots.append(SummedPlot("incl_q_from_w_from_t_flavour", [
            Plot.make1D("incl_q_from_w_from_t_0_flavour", light_from_w_from_t[0].pdgId, partonSel, EqBin(11, -5, 6)),
            Plot.make1D("incl_q_from_w_from_t_1_flavour", light_from_w_from_t[1].pdgId, partonSel, EqBin(11, -5, 6))
        ], title="Flavour of light quark from top"))

        ##### Lepton definition and scale factors
        genMuons = op.select(t.GenDressedLepton, genDefs.muonDef)
        genMuon = genMuons[0]
        genElectrons = op.select(t.GenDressedLepton, genDefs.eleDef)
        genElectron = genElectrons[0]

        genVetoMuons = op.select(t.GenDressedLepton, genDefs.vetoMuonDef)
        genVetoElectrons = op.select(t.GenDressedLepton, genDefs.vetoEleDef)

        ##### Muon and electron selection (exclusive!), merged
        genOneLepSel = genDefs.buildLeptonSelection(partonSel, genMuons, genVetoMuons, genElectrons, genVetoElectrons)

        ##### Jet definition
        genJets = genDefs.cleanJets(op.select(t.GenJet, genDefs.jetDef), genMuons, genElectrons)

        ##### B jet definition
        genBJets = op.select(genJets, lambda jet: jet.hadronFlavour == 5)
        genLightJets = op.select(genJets, lambda jet: jet.hadronFlavour != 5)

        genOneLep4JetSel = genOneLepSel.refine("lepton_4jets", cut=op.rng_len(genJets) >= 4)
        genOneLep4Jet2BSel = genOneLep4JetSel.refine("lepton_4jets_2b", cut=op.rng_len(genBJets) >= 2)

        genOneLep5JetSel = genOneLepSel.refine("lepton_5jets", cut=op.rng_len(genJets) >= 5)
        genOneLep5Jet3BSel = genOneLep5JetSel.refine("lepton_5jets_3b", cut=op.rng_len(genBJets) >= 3)

        genOneLep6JetSel = genOneLepSel.refine("lepton_6jets", cut=op.rng_len(genJets) >= 6)
        genOneLep6Jet3B1LSel = genOneLep6JetSel.refine("lepton_6jets_3b", cut=op.AND(op.rng_len(genBJets) >= 3, op.rng_len(genLightJets) > 0))
        genOneLep6Jet4BSel = genOneLep6JetSel.refine("lepton_6jets_4b", cut=op.rng_len(genBJets) >= 4)

        genOneLep7Jet4B1LSel = genOneLep6Jet4BSel.refine("lepton_7jets_4b", cut=op.AND(op.rng_len(genJets) >= 7, op.rng_len(genLightJets) > 0))

        # match genJets with quarks
        match_q_genJet_DR = 0.4
        matchedGenJets = {}
        nMatchGenJets = {}

        # match genJets with b quarks from top decays
        matchedGenJets["b0"] = op.rng_min_element_by(genJets, lambda j: op.deltaR(b_from_t[0].p4, j.p4))
        nMatchGenJets["b0"] = op.rng_count(genJets, lambda j: op.deltaR(b_from_t[0].p4, j.p4) < match_q_genJet_DR)
        matchedGenJets["b1"] = op.rng_min_element_by(genJets, lambda j: op.deltaR(b_from_t[1].p4, j.p4))
        nMatchGenJets["b1"] = op.rng_count(genJets, lambda j: op.deltaR(b_from_t[1].p4, j.p4) < match_q_genJet_DR)

        # match genJets with light quarks from top->W decays
        light_from_t = light_from_w_from_t
        matchedGenJets["l0"] = op.rng_min_element_by(genJets, lambda j: op.deltaR(light_from_t[0].p4, j.p4))
        nMatchGenJets["l0"] = op.rng_count(genJets, lambda j: op.deltaR(light_from_t[0].p4, j.p4) < match_q_genJet_DR)
        matchedGenJets["l1"] = op.rng_min_element_by(genJets, lambda j: op.deltaR(light_from_t[1].p4, j.p4))
        nMatchGenJets["l1"] = op.rng_count(genJets, lambda j: op.deltaR(light_from_t[1].p4, j.p4) < match_q_genJet_DR)

        # any other genJet
        unmatched_genJets = op.select(genJets, lambda j: op.NOT(op.OR(
            op.rng_any(b_from_t, lambda b: op.deltaR(j.p4, b.p4) < match_q_genJet_DR),
            op.rng_any(light_from_t, lambda q: op.deltaR(j.p4, q.p4) < match_q_genJet_DR))
        ))
        unmatched_genBJets = op.select(unmatched_genJets, lambda j: j.hadronFlavour == 5)
        unmatched_genLightJets = op.select(unmatched_genJets, lambda j: j.hadronFlavour != 5)

        # use GenJet branches from genHFHadronMatcher
        genJetsFromT = op.select(genBJets, lambda j: j.nBHadFromT > 0)
        genJetsFromTbar = op.select(genBJets, lambda j: j.nBHadFromTbar > 0)

        # find back the charges of the quarks
        # b0 is from a top, not anti-top
        b0_from_t_isT = op.rng_any(b_from_t[0].ancestors, lambda p: p.pdgId == 6)
        # l0 is from a top, not anti-top
        l0_from_t_isT = op.rng_any(light_from_t[0].ancestors, lambda p: p.pdgId == 6)

        # all quarks are matched to at least one jet
        match_all = op.AND(*(n > 0 for n in nMatchGenJets.values()))
        # all quarks are matched to at least one jet and the closest jet has the right flavour
        match_all_flav = op.AND(match_all, matchedGenJets["b0"].hadronFlavour == 5, matchedGenJets["b1"].hadronFlavour == 5, matchedGenJets["l0"].hadronFlavour != 5, matchedGenJets["l1"].hadronFlavour != 5)
        # all quarks are matched to a unique jet
        match_all_eq = op.AND(match_all, *( j1 != j2 for i1,j1 in enumerate(matchedGenJets.values()) for i2,j2 in enumerate(matchedGenJets.values()) if i2 > i1 ))
        # all quarks are matched to a unique jet of the right flavour
        match_all_eq_flav = op.AND(match_all_eq, match_all_flav)
        # quarks from the hadronic top are matched
        match_hadTop = op.AND(nMatchGenJets["l0"] > 0, nMatchGenJets["l1"] > 0,
                              matchedGenJets["l0"] != matchedGenJets["l1"],
                              op.switch(b0_from_t_isT == l0_from_t_isT,
                                        op.AND(nMatchGenJets["b0"] > 0, matchedGenJets["b0"] != matchedGenJets["l0"], matchedGenJets["b0"] != matchedGenJets["l1"]),
                                        op.AND(nMatchGenJets["b1"] > 0, matchedGenJets["b1"] != matchedGenJets["l0"], matchedGenJets["b1"] != matchedGenJets["l1"])
                                       ))
        match_hadTop_eq_flav = op.AND(match_hadTop, matchedGenJets["l0"].hadronFlavour != 5, matchedGenJets["l1"].hadronFlavour != 5,
                                      op.switch(b0_from_t_isT == l0_from_t_isT, matchedGenJets["b0"].hadronFlavour == 5, matchedGenJets["b1"].hadronFlavour == 5))

        # define combinations of b jets
        genBJetCouples = op.combine(genBJets, N=2)
        genExtraBJetCoupleMinDRbb = op.rng_min_element_by(genBJetCouples, lambda pair: op.deltaR(pair[0].p4, pair[1].p4))
        genExtraBJetCoupleMinMbb = op.rng_min_element_by(genBJetCouples, lambda pair: op.invariant_mass_squared(pair[0].p4, pair[1].p4))
        genExtraBJetCoupleMaxMbb = op.rng_max_element_by(genBJetCouples, lambda pair: op.invariant_mass_squared(pair[0].p4, pair[1].p4))
        maxBBSubLeadingGenJet = genExtraBJetCoupleMaxMbb[1]
        maxBBRemainingSubLeadingGenJet = op.select(genBJets, lambda j: op.AND(j != genExtraBJetCoupleMaxMbb[0], j != genExtraBJetCoupleMaxMbb[1]))[1]

        # reconstruct W and T using matched genJets
        wBoson_genJet_p4 = matchedGenJets["l0"].p4 + matchedGenJets["l1"].p4
        tQuark_genJet_p4 = wBoson_genJet_p4 + op.switch(b0_from_t_isT == l0_from_t_isT, matchedGenJets["b0"].p4, matchedGenJets["b1"].p4)

        # reconstruct W and T using invariant masses
        def genWMatchingChi2(w):
            return op.pow((op.invariant_mass(w) - 79.6) / 4.4, 2.)
        def genMatchingChi2(t, w):
            return op.pow((op.invariant_mass(t) - 169.) / 7.3, 2.) + genWMatchingChi2(w)

        genLightJetCouples = op.combine(genLightJets, N=2)
        genJet_BLL_triplets = op.combine((genBJets, genLightJetCouples), N=2)
        genJet_BLL_bestTriplet = op.rng_min_element_by(genJet_BLL_triplets, lambda trip: genMatchingChi2(trip[0].p4 + trip[1][0].p4 + trip[1][1].p4, trip[1][0].p4 + trip[1][1].p4))
        genJet_LL_recMatch = genJet_BLL_bestTriplet[1]
        genJet_B_recMatch = genJet_BLL_bestTriplet[0]
        wBoson_recGenJets_p4 = genJet_LL_recMatch[0].p4 + genJet_LL_recMatch[1].p4
        wBoson_recGenJets_correct = (wBoson_genJet_p4 == wBoson_recGenJets_p4)
        tQuark_recGenJets_p4 = wBoson_recGenJets_p4 + genJet_B_recMatch.p4
        tQuark_recGenJets_correct = (tQuark_genJet_p4 == tQuark_recGenJets_p4)
        # matching chiSq
        genRecMatch_chi2 = genMatchingChi2(tQuark_recGenJets_p4, wBoson_recGenJets_p4)
        genRecMatch_chi2Cut = genRecMatch_chi2 < 4.
        # unmatched jets
        genBJets_notRec = op.select(genBJets, lambda j: op.NOT(j == genJet_B_recMatch))
        genLightJets_notRec = op.select(genLightJets, lambda j: op.NOT(op.OR(j == genJet_LL_recMatch[0], j == genJet_LL_recMatch[1])))


        ##### gen-level Plots

        for name,sel in [("gen5j3b", genOneLep5Jet3BSel), ("gen6j4b", genOneLep6Jet4BSel)]:
            # info about quark-genJet matching
            mkPlt(name + "__b_from_t_0_nMatch", nMatchGenJets["b0"], sel, EqBin(3, 0, 3), title="Number of jets matched to b quark from top (1)")
            mkPlt(name + "__b_from_t_1_nMatch", nMatchGenJets["b1"], sel, EqBin(3, 0, 3), title="Number of jets matched to b quark from top (2)")
            mkPlt(name + "__l_from_t_0_nMatch", nMatchGenJets["l0"], sel, EqBin(3, 0, 3), title="Number of jets matched to light quark from top (1)")
            mkPlt(name + "__l_from_t_1_nMatch", nMatchGenJets["l1"], sel, EqBin(3, 0, 3), title="Number of jets matched to light quark from top (2)")
            mkPlt(name + "__isMatch_all", match_all, sel, EqBin(2, 0, 2), title="All quarks have a matching jet")
            mkPlt(name + "__isMatch_all_flav", match_all_flav, sel, EqBin(2, 0, 2), title="All quarks have a matching jet of the right flavour")
            mkPlt(name + "__isMatch_all_eq", match_all_eq, sel, EqBin(2, 0, 2), title="All quarks have a matching jet that's matched to a single quark")
            mkPlt(name + "__isMatch_all_eq_flav", match_all_eq_flav, sel, EqBin(2, 0, 2), title="All quarks have a matching jet of the right flavour that's matched to a single quark")
            mkPlt(name + "__isMatch_hadTop", match_hadTop, sel, EqBin(2, 0, 2), title="All quarks from had. top have a matching jet that's matched to a single quark")
            mkPlt(name + "__isMatch_hadTop_eq_flav", match_hadTop_eq_flav, sel, EqBin(2, 0, 2), title="All quarks from had. top. have a matching jet of the right flavour that's matched to a single quark")

            # check correspondance between genHFHadronMatcher and quark-based matching
            mkPlt(name + "__has_2b_from_ttbar", op.AND(op.rng_len(genJetsFromT) > 0, op.rng_len(genJetsFromTbar) > 0, genJetsFromT[0] != genJetsFromTbar[0]), sel, EqBin(2, 0, 2), title="Has gen jets from top and tbar from genHF matcher")
            mkPlt(name + "__n_b_from_t", op.rng_len(genJetsFromT), sel, EqBin(2, 0, 2), title="Number of genHF matcher jets from t")
            mkPlt(name + "__n_b_from_tbar", op.rng_len(genJetsFromTbar), sel, EqBin(2, 0, 2), title="Number of genHF matcher jets from tbar")
            sel_2bJetsFromTop = mkSel(sel, op.AND(op.rng_len(genJetsFromT) > 0, op.rng_len(genJetsFromTbar) > 0))
            mkPlt(name + "__b_from_t_is_tbar", genJetsFromT[0] == genJetsFromTbar[0], sel_2bJetsFromTop, EqBin(2, 0, 2), title="genHF matcher jet from t is also tbar")
            mkPlt(name + "__b_from_t_is_matched", op.OR(genJetsFromT[0] == matchedGenJets["b0"], genJetsFromT[0] == matchedGenJets["b1"]), sel_2bJetsFromTop, EqBin(2, 0, 2), title="Gen jets from top from genHF matcher is matched to b quark")
            mkPlt(name + "__b_from_tbar_is_matched", op.OR(genJetsFromTbar[0] == matchedGenJets["b0"], genJetsFromTbar[0] == matchedGenJets["b1"]), sel_2bJetsFromTop, EqBin(2, 0, 2), title="Gen jet from anti-top from genHF matcher is matched to b quark")

            plots.append(SummedPlot(name + "_genJet_from_t_hFlavour", [
                Plot.make1D(name + "_genJet_from_t_0_hFlavour", matchedGenJets["b0"].hadronFlavour, mkSel(sel, nMatchGenJets["b0"] > 0), EqBin(6, 0, 6)),
                Plot.make1D(name + "_genJet_from_t_1_hFlavour", matchedGenJets["b1"].hadronFlavour, mkSel(sel, nMatchGenJets["b1"] > 0), EqBin(6, 0, 6))
            ], title="Hadron flavour of jet matched to b quark from top"))
            plots.append(SummedPlot(name + "_genJet_from_w_from_t_hFlavour", [
                Plot.make1D(name + "_genJet_from_w_from_t_0_hFlavour", matchedGenJets["l0"].hadronFlavour, mkSel(sel, nMatchGenJets["l0"] > 0), EqBin(6, 0, 6)),
                Plot.make1D(name + "_genJet_from_w_from_t_1_hFlavour", matchedGenJets["l1"].hadronFlavour, mkSel(sel, nMatchGenJets["l1"] > 0), EqBin(6, 0, 6))
            ], title="Hadron flavour of jet matched to light quark from top"))

            plots.append(SummedPlot(name + "_b_from_t_pt", [
                Plot.make1D(name + "_b_from_t_0_pt", matchedGenJets["b0"].pt, mkSel(sel, op.AND(nMatchGenJets["b0"] > 0, matchedGenJets["b0"].hadronFlavour == 5)), EqBin(40, 20, 300)),
                Plot.make1D(name + "_b_from_t_1_pt", matchedGenJets["b1"].pt, mkSel(sel, op.AND(nMatchGenJets["b1"] > 0, matchedGenJets["b1"].hadronFlavour == 5)), EqBin(40, 20, 300))
            ], title="Pt of b jets matched to b quarks from top"))
            plots.append(SummedPlot(name + "_l_from_t_pt", [
                Plot.make1D(name + "_l_from_t_0_pt", matchedGenJets["l0"].pt, mkSel(sel, op.AND(nMatchGenJets["l0"] > 0, matchedGenJets["l0"].hadronFlavour != 5)), EqBin(40, 20, 300)),
                Plot.make1D(name + "_l_from_t_1_pt", matchedGenJets["l1"].pt, mkSel(sel, op.AND(nMatchGenJets["l1"] > 0, matchedGenJets["l1"].hadronFlavour != 5)), EqBin(40, 20, 300))
            ], title="Pt of light jets matched to light quarks from top"))

            # all jets
            mkPlt(name + "__nLightJets", op.rng_len(genLightJets), sel, EqBin(10, 0, 10), title="# light jets")
            mkPlt(name + "__nBJets", op.rng_len(genBJets), sel, EqBin(4, 3, 7), title="# b jets")
            # unmatched jets
            mkPlt(name + "__nUnmatchedJets", op.rng_len(unmatched_genJets), sel, EqBin(10, 0, 10), title="# unmatched jets")
            mkPlt(name + "__nUnmatchedBJets", op.rng_len(unmatched_genBJets), sel, EqBin(6, 0, 6), title="# unmatched b jets")
            mkPlt(name + "__nUnmatchedLightJets", op.rng_len(unmatched_genLightJets), sel, EqBin(10, 0, 10), title="# unmatched light jets")

            mkPlt(name + "__unmatched_genLightJets_pt", op.map(unmatched_genLightJets, lambda j: j.pt), sel, EqBin(40, 20, 300), title="pt of unmatched light jets")
            mkPlt(name + "__unmatched_genBJets_pt", op.map(unmatched_genBJets, lambda j: j.pt), sel, EqBin(40, 20, 300), title="pt of unmatched b jets")

            if name == "gen6j4b":
                sel_2unmatched_genBJets = mkSel(sel, op.rng_len(unmatched_genBJets) >= 2)
                mkPlt(name + "__unmatched_leading_genBJets_DRbb", op.deltaR(unmatched_genBJets[0].p4, unmatched_genBJets[1].p4), sel_2unmatched_genBJets, EqBin(40, 0, 6), title="Unmatched b jet #Delta R(bb)")
                mkPlt(name + "__unmatched_leading_genBJets_Mbb", op.invariant_mass(unmatched_genBJets[0].p4, unmatched_genBJets[1].p4), sel_2unmatched_genBJets, EqBin(40, 0, 600), title="Unmatched b jet M(bb)")
                mkPlt(name + "__unmatched_leading_genBJets_pTbb", (unmatched_genBJets[0].p4 + unmatched_genBJets[1].p4).Pt(), sel_2unmatched_genBJets, EqBin(40, 0, 400), title="Unmatched b jet pT(bb)")
                mkPlt(name + "__unmatched_leading_genBJets_DpTbb", op.abs(unmatched_genBJets[0].pt - unmatched_genBJets[1].pt), sel_2unmatched_genBJets, EqBin(40, 0, 400), title="Unmatched b jet |pt(b1)-pt(b2)|")

            # b-jets from tops
            sel_2bJets_fromTop = mkSel(sel, op.AND(nMatchGenJets["b0"] == 1, matchedGenJets["b0"].hadronFlavour == 5, nMatchGenJets["b1"] == 1, matchedGenJets["b1"].hadronFlavour == 5, matchedGenJets["b1"] != matchedGenJets["b0"]))
            mkPlt(name + "__2bJets_fromTop", sel_2bJets_fromTop.cut, sel, EqBin(2, 0, 2), title="Have 2 b jets matching 2 b quarks from tops")
            mkPlt(name + "__genBJets_from_t_DRbb", op.deltaR(matchedGenJets["b0"].p4, matchedGenJets["b1"].p4), sel_2bJets_fromTop, EqBin(40, 0, 6), title="b jets from top #Delta R(bb)")
            mkPlt(name + "__genBJets_from_t_Mbb", op.invariant_mass(matchedGenJets["b0"].p4 + matchedGenJets["b1"].p4), sel_2bJets_fromTop, EqBin(40, 0, 600), title="b jets from top M(bb)")
            mkPlt(name + "__genBJets_from_t_pTbb", (matchedGenJets["b0"].p4 + matchedGenJets["b1"].p4).Pt(), sel_2bJets_fromTop, EqBin(40, 0, 400), title="b jets from top pT(bb)")
            mkPlt(name + "__genBJets_from_t_DpTbb", op.abs(matchedGenJets["b0"].pt - matchedGenJets["b1"].pt), sel_2bJets_fromTop, EqBin(40, 0, 400), title="b jets from top |pt(b1)-pt(b2)|")

            if name == "gen6j4b":
                # closest b-jets in DeltaR
                mkPlt(name + "__minDRbb_genBJets_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMinDRbb[0]) + op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMinDRbb[1]),
                      sel, EqBin(3, 0, 3), title="#(unmatched jets): b jet pair with min. #Delta R(bb)")
                mkPlt(name + "__minDRbb_genBJets_DRbb", op.deltaR(genExtraBJetCoupleMinDRbb[0].p4, genExtraBJetCoupleMinDRbb[1].p4), sel, EqBin(40, 0, 3), title="#Delta R(bb): b jet pair with min. #Delta R(bb)")
                mkPlt(name + "__minDRbb_genBJets_Mbb", op.invariant_mass(genExtraBJetCoupleMinDRbb[0].p4, genExtraBJetCoupleMinDRbb[1].p4), sel, EqBin(40, 0, 300), title="M(bb): b jet pair with min. #Delta R(bb)")
                mkPlt(name + "__minDRbb_genBJets_pTbb", (genExtraBJetCoupleMinDRbb[0].p4 + genExtraBJetCoupleMinDRbb[1].p4).Pt(), sel, EqBin(40, 0, 400), title="pT(bb): b jet pair with min. #Delta R(bb)")

                # b-jets with largest invariant mass
                mkPlt(name + "__maxMbb_genBJets_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMaxMbb[0]) + op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMaxMbb[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched jets): b jet pair with max. M(bb)")
                mkPlt(name + "__maxMbb_genBJets_DRbb", op.deltaR(genExtraBJetCoupleMaxMbb[0].p4, genExtraBJetCoupleMaxMbb[1].p4), sel, EqBin(40, 0, 6), title="#Delta R(bb): b jet pair with max. #Delta R(bb)")
                mkPlt(name + "__maxMbb_genBJets_Mbb", op.invariant_mass(genExtraBJetCoupleMaxMbb[0].p4, genExtraBJetCoupleMaxMbb[1].p4), sel, EqBin(40, 0, 1000), title="M(bb): b jet pair with max. #Delta R(bb)")
                mkPlt(name + "__maxMbb_genBJets_pTbb", (genExtraBJetCoupleMaxMbb[0].p4 + genExtraBJetCoupleMaxMbb[1].p4).Pt(), sel, EqBin(40, 0, 400), title="pT(bb): b jet pair with max. #Delta R(bb)")

                # b-jets with smallest invariant mass
                mkPlt(name + "__minMbb_genBJets_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMinMbb[0]) + op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMinMbb[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched jets): b jet pair with min. M(bb)")
                mkPlt(name + "__minMbb_genBJets_DRbb", op.deltaR(genExtraBJetCoupleMinMbb[0].p4, genExtraBJetCoupleMinMbb[1].p4), sel, EqBin(40, 0, 3), title="#Delta R(bb): b jet pair with min. M(bb)")
                mkPlt(name + "__minMbb_genBJets_Mbb", op.invariant_mass(genExtraBJetCoupleMinMbb[0].p4, genExtraBJetCoupleMinMbb[1].p4), sel, EqBin(40, 0, 200), title="M(bb): b jet pair with min. M(bb)")
                mkPlt(name + "__minMbb_genBJets_pTbb", (genExtraBJetCoupleMinMbb[0].p4 + genExtraBJetCoupleMinMbb[1].p4).Pt(), sel, EqBin(40, 0, 400), title="pT(bb): b jet pair with min. M(bb)")

                # leading b-jets
                mkPlt(name + "__leading_genBJets_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == genBJets[0]) + op.rng_any(unmatched_genBJets, lambda j: j == genBJets[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched jets): leading + subleading b jets")
                mkPlt(name + "__leading_genBJets_DRbb", op.deltaR(genBJets[0].p4, genBJets[1].p4), sel, EqBin(40, 0, 6), title="#Delta R(bb): 1st and 2nd by pt")
                mkPlt(name + "__leading_genBJets_Mbb", op.invariant_mass(genBJets[0].p4, genBJets[1].p4), sel, EqBin(20, 0, 1000), title="M(bb): 1st and 2nd by pt")
                mkPlt(name + "__leading_genBJets_pTbb", (genBJets[0].p4 + genBJets[1].p4).Pt(), sel, EqBin(40, 0, 400), title="pT(bb): 1st and 2nd by pt")

                # use Max(bb)
                mkPlt(name + "__maxMbbTwist_bJets_notRec_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == maxBBSubLeadingGenJet) + op.rng_any(unmatched_genBJets, lambda j: j == maxBBRemainingSubLeadingGenJet),
                         sel, EqBin(3, 0, 3), title="#(unmatched jets) in remaining (**), max M(bb) pair")

                # average DRbb
                mkPlt(name + "__average_DRbb", op.rng_mean(op.map(genBJetCouples, lambda p: op.deltaR(p[0].p4, p[1].p4))), sel, EqBin(40, 0.6, 4), title="Average #Delta R(bb)")

            # 3rd and 4th subleading b-jets
            if name == "gen5j3b":
                mkPlt(name + "__subleading3_genBJet_isUnMatched", op.rng_any(unmatched_genBJets, lambda j: j == genBJets[2]), sel, EqBin(2, 0, 2), title="3rd b jet is unmatched?")
            elif name == "gen6j4b":
                mkPlt(name + "__subleading34_genBJets_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == genBJets[2]) + op.rng_any(unmatched_genBJets, lambda j: j == genBJets[3]),
                      sel, EqBin(3, 0, 3), title="#(unmatched jets): 3rd and 4th b jet")
                mkPlt(name + "__subleading34_genBJets_DRbb", op.deltaR(genBJets[2].p4, genBJets[3].p4), sel, EqBin(40, 0, 6), title="#Delta R(bb): 3rd and 4th by pt")
                mkPlt(name + "__subleading34_genBJets_Mbb", op.invariant_mass(genBJets[2].p4, genBJets[3].p4), sel, EqBin(20, 0, 300), title="M(bb): 3rd and 4th by pt")
                mkPlt(name + "__subleading34_genBJets_pTbb", (genBJets[2].p4 + genBJets[3].p4).Pt(), sel, EqBin(40, 0, 300), title="pT(bb): 3rd and 4th by pt")

            # for top matching we need at least 2 light jets
            sel = mkSel(sel, op.rng_len(genLightJets) >= 2)

            mkPlt(name + "__genRecMatch_chi2", genRecMatch_chi2, sel, EqBin(40, 0, 20), title="Had. top matching Chi2")

            # W and t true masses - if matching jets exist
            sel_hadTop_eq_flav = mkSel(sel, match_hadTop_eq_flav)
            mkPlt(name + "_match_hadTop_eq_flav__trueMatchedJets_W_mass", op.invariant_mass(wBoson_genJet_p4), sel_hadTop_eq_flav, EqBin(80, 50, 110), title="M(W) from correctly matched jets")
            mkPlt(name + "_match_hadTop_eq_flav__trueMatchedJets_t_mass", op.invariant_mass(tQuark_genJet_p4), sel_hadTop_eq_flav, EqBin(80, 120, 220), title="True m(t) from correctly matched jets")

            # correct matching after chi2 cut? - if matching jets exist
            sel_hadTop_eq_flav_chi2 = mkSel(sel, genRecMatch_chi2Cut)
            mkPlt(name + "_match_hadTop_eq_flav_chi2__recJets_W_correct", wBoson_recGenJets_correct, sel_hadTop_eq_flav_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. W?")
            mkPlt(name + "_match_hadTop_eq_flav_chi2__recJets_t_correct", tQuark_recGenJets_correct, sel_hadTop_eq_flav_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. top?")

            # correct matching after chi2 cut?
            sel = mkSel(sel, genRecMatch_chi2Cut)
            
            mkPlt(name + "_genRecMatchChi2__recJets_W_correct", op.AND(match_hadTop_eq_flav, wBoson_recGenJets_correct), sel, EqBin(2, 0, 2), title="Correct jets matched to had. W?")
            mkPlt(name + "_genRecMatchChi2__recJets_t_correct", op.AND(match_hadTop_eq_flav, tQuark_recGenJets_correct), sel, EqBin(2, 0, 2), title="Correct jets matched to had. top?")

            # jets not matched using chi2
            if name == "gen5j3b":
                mkPlt(name + "_genRecMatchChi2__leading_bJet_notRec_isUnMatched", op.rng_any(unmatched_genBJets, lambda j: j == genBJets_notRec[0]), sel, EqBin(2, 0, 2), title="Leading remaining b jet is unmatched?")
                mkPlt(name + "_genRecMatchChi2__subleading_bJet_notRec_isUnMatched", op.rng_any(unmatched_genBJets, lambda j: j == genBJets_notRec[1]), sel, EqBin(2, 0, 2), title="Subleading remaining b jet is unmatched?")
            elif name == "gen6j4b":
                # define combinations of b jets using unmatched jets only
                genBJetCouples_notRec = op.combine(genBJets_notRec, N=2)
                genExtraBJetCoupleMinDRbb_notRec = op.rng_min_element_by(genBJetCouples_notRec, lambda pair: op.deltaR(pair[0].p4, pair[1].p4))
                genExtraBJetCoupleMinMbb_notRec = op.rng_min_element_by(genBJetCouples_notRec, lambda pair: op.invariant_mass_squared(pair[0].p4, pair[1].p4))
                genExtraBJetCoupleMaxMbb_notRec = op.rng_max_element_by(genBJetCouples_notRec, lambda pair: op.invariant_mass_squared(pair[0].p4, pair[1].p4))
                maxBBnotRecSubLeadingGenJet = genExtraBJetCoupleMaxMbb_notRec[1]
                maxBBRemainingNotRecLeadingGenJet = op.select(genBJets_notRec, lambda j: op.AND(j != genExtraBJetCoupleMaxMbb_notRec[0], j != genExtraBJetCoupleMaxMbb_notRec[1]))[0]

                mkPlt(name + "_genRecMatchChi2__leading_bJets_notRec_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == genBJets_notRec[0]) + op.rng_any(unmatched_genBJets, lambda j: j == genBJets_notRec[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched jets) in remaining, 1st and 2nd")
                mkPlt(name + "_genRecMatchChi2__subleading23_bJets_notRec_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == genBJets_notRec[1]) + op.rng_any(unmatched_genBJets, lambda j: j == genBJets_notRec[2]),
                         sel, EqBin(3, 0, 3), title="#(unmatched jets) in remaining, 2nd and 3rd")
                mkPlt(name + "_genRecMatchChi2__minDRbb_bJets_notRec_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMinDRbb_notRec[0]) + op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMinDRbb_notRec[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched jets) in remaining, min #Delta R(bb) pair")
                mkPlt(name + "_genRecMatchChi2__maxMbb_bJets_notRec_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMaxMbb_notRec[0]) + op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMaxMbb_notRec[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched jets) in remaining, max M(bb) pair")
                mkPlt(name + "_genRecMatchChi2__minMbb_bJets_notRec_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMinMbb_notRec[0]) + op.rng_any(unmatched_genBJets, lambda j: j == genExtraBJetCoupleMinMbb_notRec[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched jets) in remaining, min M(bb) pair")
                mkPlt(name + "_genRecMatchChi2__maxMbbTwist_bJets_notRec_areUnMatched",
                         op.rng_any(unmatched_genBJets, lambda j: j == maxBBnotRecSubLeadingGenJet) + op.rng_any(unmatched_genBJets, lambda j: j == maxBBRemainingNotRecLeadingGenJet),
                         sel, EqBin(3, 0, 3), title="#(unmatched jets) in remaining (**), max M(bb) pair")


        for name,sel in [("gen6j3b1l", genOneLep6Jet3B1LSel), ("gen7j4b1l", genOneLep7Jet4B1LSel)]:
            plots.append(SummedPlot(name + "_l_from_t_pt", [
                Plot.make1D(name + "_l_from_t_0_pt", matchedGenJets["l0"].pt, mkSel(sel, op.AND(nMatchGenJets["l0"] > 0, matchedGenJets["l0"].hadronFlavour != 5)), EqBin(40, 20, 300)),
                Plot.make1D(name + "_l_from_t_1_pt", matchedGenJets["l1"].pt, mkSel(sel, op.AND(nMatchGenJets["l1"] > 0, matchedGenJets["l1"].hadronFlavour != 5)), EqBin(40, 20, 300))
            ], title="Pt of light jets matched to light quarks from top"))

            mkPlt(name + "__nUnmatchedLightJets", op.rng_len(unmatched_genLightJets), sel, EqBin(10, 0, 10), title="# unmatched jets")

            # have at least one unmatched light jet
            mkPlt(name + "__leading_unmatched_genLightJet_pt", unmatched_genLightJets[0].pt, mkSel(sel, op.rng_len(unmatched_genLightJets) > 0), EqBin(40, 20, 300), title="pt of leading unmatched light jet")

            mkPlt(name + "__unmatched_genLightJets_pt", op.map(unmatched_genLightJets, lambda j: j.pt), sel, EqBin(40, 20, 300), title="pt of unmatched light jets")
            mkPlt(name + "__leading_genLightJet_isUnMatched", op.rng_any(unmatched_genLightJets, lambda j: j == genLightJets[0]), sel, EqBin(2, 0, 2), title="Leading light jet is unmatched?")

            # top rec matching: need 2 light jets
            sel = mkSel(sel, op.rng_len(genLightJets) >= 2)

            sel_chi2 = mkSel(sel, genRecMatch_chi2Cut)
            mkPlt(name + "_genRecMatchChi2__recJets_W_correct", wBoson_recGenJets_correct, sel_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. W?")
            mkPlt(name + "_genRecMatchChi2__recJets_t_correct", tQuark_recGenJets_correct, sel_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. top?")
            
            sel_hadTop_eq_flav_chi2 = sel_chi2.refine(sel_chi2.name + "_hadTop_eq_flav_chi2", cut=match_hadTop_eq_flav)
            mkPlt(name + "_match_hadTop_eq_flav_chi2__recJets_W_correct", wBoson_recGenJets_correct, sel_hadTop_eq_flav_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. W?")
            mkPlt(name + "_match_hadTop_eq_flav_chi2__recJets_t_correct", tQuark_recGenJets_correct, sel_hadTop_eq_flav_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. top?")

            # look at extra light jet on top of the two from the top -> need 3 light jets
            sel = mkSel(sel, op.rng_len(genLightJets) >= 3)
            mkPlt(name + "__3rd_genLightJets_pt", genLightJets[2].pt, sel, EqBin(40, 20, 200), title="pt of 3rd light jet")
            mkPlt(name + "__3rd_genLightJet_isUnMatched", op.rng_any(unmatched_genLightJets, lambda j: j == genLightJets[2]), sel, EqBin(2, 0, 2), title="3rd light jet is unmatched?")

            recSel = mkSel(sel, op.AND(genRecMatch_chi2Cut, op.rng_len(genLightJets_notRec) > 0))
            mkPlt(name + "_genRecMatchChi2__leading_genLightJet_isUnMatched", op.rng_any(unmatched_genLightJets, lambda j: j == genLightJets_notRec[0]), recSel, EqBin(2, 0, 2), title="Leading remaining light jet is unmatched?")


        ##### Reco definition and selections

        bTagWP = "M"
        def isTagB(jet):
            return (jet.btagDeepFlavB >= defs.bTagWorkingPoints[era]["btagDeepFlavB"][bTagWP])
        def isTagL(jet):
            return op.NOT(isTagB(jet))
        def isTrueB(jet):
            return (jet.hadronFlavour == 5)
        def isTrueL(jet):
            return op.NOT(isTrueB(jet))

        # Only do the category splitting at reco level!!
        if "subprocess" in sampleCfg:
            recoSel = utils.splitTTjetFlavours(sampleCfg, t, noSel)
        else:
            recoSel = noSel
        recoSel = recoSel.refine("flags", cut=defs.flagDef(t.Flag, era, True), weight=defs.makePUWeight(t, era))
        muons = op.select(t.Muon, defs.muonDef(era))
        muon = muons[0]
        electrons = op.select(t.Electron, defs.eleDef(era))
        electron = electrons[0]
        vetoMuons = op.select(t.Muon, defs.vetoMuonDef)
        vetoElectrons = op.select(t.Electron, defs.vetoEleDef)
        _,oneMuTriggerSel = defs.buildMuonSelections(t, recoSel, muons, vetoMuons, electrons, vetoElectrons, sample, era, True)
        _,oneEleTriggerSel = defs.buildElectronSelections(t, recoSel, muons, vetoMuons, electrons, vetoElectrons, sample, era, True)
        # cheat a bit: define a merged one-lepton selection
        oneLepTriggerSel = recoSel.refine("lep", cut=op.OR(oneMuTriggerSel.cut, oneEleTriggerSel.cut))
        cleanedJets = defs.cleanJets(op.select(t.Jet, defs.jetDef), muons, electrons)
        bJets = op.select(cleanedJets, lambda j: isTagB(j))
        bJetsByDeepFlav = op.sort(bJets, lambda j: -j.btagDeepFlavB)
        lightJets = op.select(cleanedJets, lambda j: isTagL(j))
        oneLep5Jet3BSel = oneLepTriggerSel.refine("lep_5j_3b", cut=[op.rng_len(cleanedJets) >= 5, op.rng_len(bJets) >= 3], weight=[ op.rng_product(cleanedJets, lambda jet: jet.btagSF_deepjet_shape) ])
        oneLep6Jet3B1LSel = oneLep5Jet3BSel.refine("lep_6j_3b_1l", cut=op.AND(op.rng_len(cleanedJets) >= 6, op.rng_len(lightJets) >= 1))
        oneLep6Jet4BSel = oneLep5Jet3BSel.refine("lep_6j_4b", cut=op.AND(op.rng_len(cleanedJets) >= 6, op.rng_len(bJets) >= 4))
        oneLep7Jet4B1LSel = oneLep6Jet4BSel.refine("lep_7j_4b_1l", cut=op.AND(op.rng_len(cleanedJets) >= 7, op.rng_len(lightJets) >= 1))

        # match jets to genJets (themselves matched to quarks from top)
        match_jet_genJet_DR = 0.4
        matchedJetColls = {}
        matchedJets = {}
        nMatchJets = {}

        def matchJetToGenJet(jet, genJet, nMatchGenJet):
            return op.AND(jet.genJet.idx >= 0, nMatchGenJet > 0, jet.genJet == genJet, op.deltaR(jet.p4, jet.genJet.p4) < match_jet_genJet_DR)

        for q in matchedGenJets.keys():
            matchedJetColls[q] = op.select(cleanedJets, lambda j: matchJetToGenJet(j, matchedGenJets[q], nMatchGenJets[q]))
            nMatchJets[q] = op.rng_len(matchedJetColls[q])
            matchedJets[q] = matchedJetColls[q][0]

        recoJets_matchAll = op.AND(*(n > 0 for n in nMatchJets.values()),
                                    *( j1 != j2 for i1,j1 in enumerate(matchedJets.values()) for i2,j2 in enumerate(matchedJets.values()) if i2 > i1 ))
        recoJets_matchAll_flav = op.AND(recoJets_matchAll, isTagB(matchedJets["b0"]), isTagB(matchedJets["b1"]), isTagL(matchedJets["l0"]), isTagL(matchedJets["l1"]))
        # quarks from the hadronic top are matched
        recoJets_match_hadTop = op.AND(nMatchJets["l0"] > 0, nMatchJets["l1"] > 0, op.switch(b0_from_t_isT == l0_from_t_isT, nMatchJets["b0"] > 0, nMatchJets["b1"] > 0))
        recoJets_match_hadTop_flav = op.AND(recoJets_match_hadTop,
                                            isTagL(matchedJets["l0"]), isTagL(matchedJets["l1"]),
                                            op.switch(b0_from_t_isT == l0_from_t_isT, isTagB(matchedJets["b0"]), isTagB(matchedJets["b1"])))

        # reconstruct W and T using matched jets
        wBoson_jet_p4 = matchedJets["l0"].p4 + matchedJets["l1"].p4
        tQuark_jet_p4 = wBoson_jet_p4 + op.switch(b0_from_t_isT == l0_from_t_isT, matchedJets["b0"].p4, matchedJets["b1"].p4)

        unmatched_jets = op.select(cleanedJets, lambda j: op.NOT(op.OR(*(matchJetToGenJet(j, matchedGenJets[q], nMatchGenJets[q]) for q in matchedGenJets.keys()))))
        unmatched_bJets = op.select(unmatched_jets, lambda j: isTagB(j))
        unmatched_bJets_true = op.select(unmatched_bJets, lambda j: isTrueB(j))
        unmatched_lightJets = op.select(unmatched_jets, lambda j: isTagL(j))
        unmatched_lightJets_true = op.select(unmatched_lightJets, lambda j: isTrueL(j))

        bJetCouples = op.combine(bJets, N=2)
        extraBJetCoupleMinDRbb = op.rng_min_element_by(bJetCouples, lambda pair: op.deltaR(pair[0].p4, pair[1].p4))
        extraBJetCoupleMinMbb = op.rng_min_element_by(bJetCouples, lambda pair: op.invariant_mass_squared(pair[0].p4, pair[1].p4))
        extraBJetCoupleMaxMbb = op.rng_max_element_by(bJetCouples, lambda pair: op.invariant_mass_squared(pair[0].p4, pair[1].p4))

        # reconstruct W and T using invariant masses
        def wMatchingChi2(w):
            return op.pow((op.invariant_mass(w) - 82.8) / 11.7, 2.)
        def matchingChi2(t, w):
            return op.pow((op.invariant_mass(t) - 171.) / 20.3, 2.) + wMatchingChi2(w)

        lightJetCouples = op.combine(lightJets, N=2)
        jets_LLB_triplets = op.combine((bJets, lightJetCouples), N=2)
        jets_LLB_bestTriplet = op.rng_min_element_by(jets_LLB_triplets, lambda trip: matchingChi2(trip[0].p4 + trip[1][0].p4 + trip[1][1].p4, trip[1][0].p4 + trip[1][1].p4))
        jets_LL_Wmass = jets_LLB_bestTriplet[1]
        bJet_WTmasses = jets_LLB_bestTriplet[0]
        wBoson_recJets_p4 = jets_LL_Wmass[0].p4 + jets_LL_Wmass[1].p4
        wBoson_recJets_correct = (wBoson_jet_p4 == wBoson_recJets_p4)
        tQuark_recJets_p4 = wBoson_recJets_p4 + bJet_WTmasses.p4
        tQuark_recJets_correct = (tQuark_jet_p4 == tQuark_recJets_p4)
        # matching chiSq
        recMatch_chi2 = matchingChi2(tQuark_recJets_p4, wBoson_recJets_p4)
        recMatch_chi2Cut = recMatch_chi2 < 8.
        # unmatched jets
        bJets_notRec = op.select(bJets, lambda j: op.NOT(j == bJet_WTmasses))
        lightJets_notRec = op.select(lightJets, lambda j: op.NOT(op.OR(j == jets_LL_Wmass[0], j == jets_LL_Wmass[1])))
        # define combinations of b jets using unmatched jets only
        bJetCouples_notRec = op.combine(bJets_notRec, N=2)
        extraBJetCoupleMinDRbb_notRec = op.rng_min_element_by(bJetCouples_notRec, lambda pair: op.deltaR(pair[0].p4, pair[1].p4))
        extraBJetCoupleMinMbb_notRec = op.rng_min_element_by(bJetCouples_notRec, lambda pair: op.invariant_mass_squared(pair[0].p4, pair[1].p4))
        extraBJetCoupleMaxMbb_notRec = op.rng_max_element_by(bJetCouples_notRec, lambda pair: op.invariant_mass_squared(pair[0].p4, pair[1].p4))
        maxBBnotRecSubLeadingJet = extraBJetCoupleMaxMbb_notRec[1]
        maxBBRemainingNotRecLeadingJet = op.select(bJets_notRec, lambda j: op.AND(j != extraBJetCoupleMaxMbb_notRec[0], j != extraBJetCoupleMaxMbb_notRec[1]))[0]
        extraBJetCoupleMaxBTag_notRec = op.rng_max_element_by(bJetCouples_notRec, lambda pair: pair[0].btagDeepFlavB + pair[1].btagDeepFlavB)

        # W matching ONLY
        jets_LL_wOnly_best = op.rng_min_element_by(lightJetCouples, lambda pair: wMatchingChi2(pair[0].p4 + pair[1].p4))
        jets_LL_wOnly_best_p4 = jets_LL_wOnly_best[0].p4 + jets_LL_wOnly_best[1].p4
        wBoson_wOnly_recJets_correct = (wBoson_jet_p4 == (jets_LL_wOnly_best_p4))
        lightJets_wOnly_notRec = op.select(lightJets, lambda j: op.NOT(op.OR(j == jets_LL_wOnly_best[0], j == jets_LL_wOnly_best[1])))

        ##### reco-level Plots

        for name,sel in [("5j3b", oneLep5Jet3BSel), ("6j4b", oneLep6Jet4BSel)]:
            if name == "5j3b":
                genSel = sel.refine(sel.name + "_gen", cut=genOneLep5Jet3BSel.cut)
                mkPlt(name + "__genSel_1l5j", mkSel(sel, genOneLep5JetSel.cut).cut, sel, EqBin(2, 0, 2), title="Pass gen-level PS: 1l5j")
            if name == "6j4b":
                genSel = sel.refine(sel.name + "_gen", cut=genOneLep6Jet4BSel.cut)
                mkPlt(name + "__genSel_1l6j", mkSel(sel, genOneLep6JetSel.cut).cut, sel, EqBin(2, 0, 2), title="Pass gen-level PS: 1l6j")
            mkPlt(name + "__genSel", genSel.cut, sel, EqBin(2, 0, 2), title="Pass gen-level PS")
            mkPlt(name + "__genSel_1l", mkSel(sel, genOneLepSel.cut).cut, sel, EqBin(2, 0, 2), title="Pass gen-level PS: 1l")
            mkPlt(name + "__genSel_1l4j", mkSel(sel, genOneLep4JetSel.cut).cut, sel, EqBin(2, 0, 2), title="Pass gen-level PS: 1l4j")
            mkPlt(name + "__genSel_1l4j2b", mkSel(sel, genOneLep4Jet2BSel.cut).cut, sel, EqBin(2, 0, 2), title="Pass gen-level PS: 1l4j2b")

            # info about jet-genJet matching
            mkPlt(name + "__b_from_t_0_nMatch", nMatchJets["b0"], sel, EqBin(3, 0, 3), title="Number of jets matched to b jet from top (1)")
            mkPlt(name + "__b_from_t_1_nMatch", nMatchJets["b1"], sel, EqBin(3, 0, 3), title="Number of jets matched to b jet from top (2)")
            mkPlt(name + "__l_from_t_0_nMatch", nMatchJets["l0"], sel, EqBin(3, 0, 3), title="Number of jets matched to light jet from top (1)")
            mkPlt(name + "__l_from_t_1_nMatch", nMatchJets["l1"], sel, EqBin(3, 0, 3), title="Number of jets matched to light jet from top (2)")

            for n,s in [(name,sel), (name+"_gen", genSel)]:
                mkPlt(n + "__matchQuarks_all_eq", match_all_eq, s, EqBin(2, 0, 2), title="Match all genJets <-> quarks")
                mkPlt(n + "__matchQuarks_matchJets_all_eq", op.AND(match_all_eq, recoJets_matchAll), s, EqBin(2, 0, 2), title="Match all jets <-> genJets <-> quarks")
                mkPlt(n + "__matchQuarks_all_eq_flav", match_all_eq_flav, s, EqBin(2, 0, 2), title="Match all genJets <-> quarks (+flav)")
                mkPlt(n + "__matchQuarks_matchJets_all_eq_flav", op.AND(match_all_eq_flav, recoJets_matchAll_flav), s, EqBin(2, 0, 2), title="Match all jets <-> genJets <-> quarks (+flav)")
                mkPlt(n + "__matchQuarks_hadTop", match_hadTop, s, EqBin(2, 0, 2), title="Match had. top genJets <-> quarks")
                mkPlt(n + "__matchQuarks_matchJets_hadTop", op.AND(match_hadTop, recoJets_match_hadTop), s, EqBin(2, 0, 2), title="Match had. top jets <-> genJets <-> quarks")
                mkPlt(n + "__matchQuarks_hadTop_flav", match_hadTop_eq_flav, s, EqBin(2, 0, 2), title="Match had. top genJets <-> quarks (+flav)")
                mkPlt(n + "__matchQuarks_matchJets_hadTop_flav", op.AND(match_hadTop_eq_flav, recoJets_match_hadTop_flav), s, EqBin(2, 0, 2), title="Match had. top jets <-> genJets <-> quarks (+flav)")

            mkPlt(name + "__jet_genJet_DR", op.map(op.select(cleanedJets, lambda j: j.genJet.idx >= 0), lambda j: op.deltaR(j.p4, j.genJet.p4)), sel, EqBin(40, 0, 6), title="#Delta R between jet and matched genJet")

            # b tagging performance
            if name == "5j3b":
                mkPlt(name + "__bTaggedAreTrueB", op.rng_count(bJetsByDeepFlav[:3], lambda j: isTrueB(j)), sel, EqBin(4, 0, 4), title="# true b in 3 deepFlav-leading b-tagged jets")
                mkPlt(name + "__lTaggedAreTrueL", op.rng_count(lightJets, lambda j: isTrueL(j))/op.rng_len(lightJets), sel, EqBin(21, 0, 1.05), title="Fraction of true lights in all light-tagged jets")
            if name == "6j4b":
                mkPlt(name + "__bTaggedAreTrueB", op.rng_count(bJetsByDeepFlav[:4], lambda j: isTrueB(j)), sel, EqBin(5, 0, 5), title="# true b in 4 deepFlav-leading b-tagged jets")
                mkPlt(name + "__lTaggedAreTrueL", op.rng_count(lightJets, lambda j: isTrueL(j))/op.rng_len(lightJets), sel, EqBin(21, 0, 1.05), title="Fraction of true lights in all light-tagged jets")

            # unmatched jets
            mkPlt(name + "__nUnmatchedJets", op.rng_len(unmatched_jets), sel, EqBin(10, 0, 10), title="# unmatched jets")
            mkPlt(name + "__nUnmatchedBJets", op.rng_len(unmatched_bJets), sel, EqBin(6, 0, 6), title="# unmatched b jets")
            mkPlt(name + "__nUnmatchedLightJets", op.rng_len(unmatched_lightJets), sel, EqBin(10, 0, 10), title="# unmatched light jets")
            mkPlt(name + "__nUnmatchedTrueBJets", op.rng_len(unmatched_bJets_true), sel, EqBin(6, 0, 6), title="# unmatched b jets that are true")
            mkPlt(name + "__nUnmatchedTrueLightJets", op.rng_len(unmatched_lightJets_true), sel, EqBin(10, 0, 10), title="# unmatched light jets that are true")

            mkPlt(name + "__unmatched_trueLightJets_pt", op.map(unmatched_lightJets_true, lambda j: j.pt), sel, EqBin(40, 30, 300), title="pt of unmatched true light jets")
            mkPlt(name + "__unmatched_trueBJets_pt", op.map(unmatched_bJets_true, lambda j: j.pt), sel, EqBin(40, 30, 300), title="pt of unmatched true b jets")

            plots.append(SummedPlot(name + "_b_from_t_pt", [
                Plot.make1D(name + "_b_from_t_0_pt", matchedJets["b0"].pt, mkSel(sel, op.AND(nMatchJets["b0"] > 0, isTagB(matchedJets["b0"]))), EqBin(40, 30, 300)),
                Plot.make1D(name + "_b_from_t_1_pt", matchedJets["b1"].pt, mkSel(sel, op.AND(nMatchJets["b1"] > 0, isTagB(matchedJets["b1"]))), EqBin(40, 30, 300)),
            ], title="Pt of b jets matched to b quarks from top"))
            plots.append(SummedPlot(name + "_l_from_t_pt", [
                Plot.make1D(name + "_l_from_t_0_pt", matchedJets["l0"].pt, mkSel(sel, op.AND(nMatchJets["l0"] > 0, isTagL(matchedJets["l0"]))), EqBin(40, 30, 300)),
                Plot.make1D(name + "_l_from_t_1_pt", matchedJets["l1"].pt, mkSel(sel, op.AND(nMatchJets["l1"] > 0, isTagL(matchedJets["l1"]))), EqBin(40, 30, 300)),
            ], title="Pt of light jets matched to light quarks from top"))

            if name == "6j4b":
                sel_2unmatched_bJets = mkSel(sel, op.rng_len(unmatched_bJets_true) >= 2)
                mkPlt(name + "__unmatched_leading_trueBJets_DRbb", op.deltaR(unmatched_bJets_true[0].p4, unmatched_bJets_true[1].p4), sel_2unmatched_bJets, EqBin(40, 0, 6), title="Unmatched b jet #Delta R(bb)")
                mkPlt(name + "__unmatched_leading_trueBJets_Mbb", op.invariant_mass(unmatched_bJets_true[0].p4, unmatched_bJets_true[1].p4), sel_2unmatched_bJets, EqBin(40, 0, 600), title="Unmatched b jet M(bb)")
                mkPlt(name + "__unmatched_leading_trueBJets_pTbb", (unmatched_bJets_true[0].p4 + unmatched_bJets_true[1].p4).Pt(), sel_2unmatched_bJets, EqBin(40, 0, 400), title="Unmatched b jet pT(bb)")
                mkPlt(name + "__unmatched_leading_trueBJets_DpTbb", op.abs(unmatched_bJets_true[0].pt - unmatched_bJets_true[1].pt), sel_2unmatched_bJets, EqBin(40, 0, 400), title="Unmatched b jet |pt(b1)-pt(b2)|")

            # b-jets from tops
            sel_2bJets_fromTop = mkSel(sel, op.AND(
                nMatchGenJets["b0"] > 0, nMatchJets["b0"] > 0, isTagB(matchedJets["b0"]), isTrueB(matchedJets["b0"]),
                nMatchGenJets["b1"] > 0, nMatchJets["b1"] > 0, isTagB(matchedJets["b1"]), isTrueB(matchedJets["b1"]),
                matchedJets["b0"] != matchedJets["b1"]))
            mkPlt(name + "__2bJets_fromTop", sel_2bJets_fromTop.cut, sel, EqBin(2, 0, 2), title="Have 2 true b jets matching 2 b genJets from tops")
            mkPlt(name + "__bJets_from_t_DRbb", op.deltaR(matchedJets["b0"].p4, matchedJets["b1"].p4), sel_2bJets_fromTop, EqBin(40, 0, 6), title="True b jets from top #Delta R(bb)")
            mkPlt(name + "__bJets_from_t_Mbb", op.invariant_mass(matchedJets["b0"].p4 + matchedJets["b1"].p4), sel_2bJets_fromTop, EqBin(40, 0, 600), title="True b jets from top M(bb)")
            mkPlt(name + "__bJets_from_t_pTbb", (matchedJets["b0"].p4 + matchedJets["b1"].p4).Pt(), sel_2bJets_fromTop, EqBin(40, 0, 400), title="True b jets from top pT(bb)")
            mkPlt(name + "__bJets_from_t_DpTbb", op.abs(matchedJets["b0"].pt - matchedJets["b1"].pt), sel_2bJets_fromTop, EqBin(40, 0, 400), title="True b jets from top |pt(b1)-pt(b2)|")

            if name == "6j4b":
                # closest b-jets in DeltaR
                mkPlt(name + "__minDRbb_bJets_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMinDRbb[0]) + op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMinDRbb[1]),
                      sel, EqBin(3, 0, 3), title="#(unmatched true b jets): b jet pair with min. #Delta R(bb)")
                mkPlt(name + "__minDRbb_bJets_DRbb", op.deltaR(extraBJetCoupleMinDRbb[0].p4, extraBJetCoupleMinDRbb[1].p4), sel, EqBin(40, 0, 3), title="#Delta R(bb): b jet pair with min. #Delta R(bb)")
                mkPlt(name + "__minDRbb_bJets_Mbb", op.invariant_mass(extraBJetCoupleMinDRbb[0].p4, extraBJetCoupleMinDRbb[1].p4), sel, EqBin(40, 0, 300), title="M(bb): b jet pair with min. #Delta R(bb)")
                mkPlt(name + "__minDRbb_bJets_pTbb", (extraBJetCoupleMinDRbb[0].p4 + extraBJetCoupleMinDRbb[1].p4).Pt(), sel, EqBin(40, 0, 400), title="pT(bb): b jet pair with min. #Delta R(bb)")

                # b-jets with largest invariant mass
                mkPlt(name + "__maxMbb_bJets_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMaxMbb[0]) + op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMaxMbb[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched true b jets): b jet pair with max. M(bb)")
                mkPlt(name + "__maxMbb_bJets_DRbb", op.deltaR(extraBJetCoupleMaxMbb[0].p4, extraBJetCoupleMaxMbb[1].p4), sel, EqBin(40, 0, 6), title="#Delta R(bb): b jet pair with max. #Delta R(bb)")
                mkPlt(name + "__maxMbb_bJets_Mbb", op.invariant_mass(extraBJetCoupleMaxMbb[0].p4, extraBJetCoupleMaxMbb[1].p4), sel, EqBin(40, 0, 1000), title="M(bb): b jet pair with max. #Delta R(bb)")
                mkPlt(name + "__maxMbb_bJets_pTbb", (extraBJetCoupleMaxMbb[0].p4 + extraBJetCoupleMaxMbb[1].p4).Pt(), sel, EqBin(40, 0, 400), title="pT(bb): b jet pair with max. #Delta R(bb)")

                # b-jets with smallest invariant mass
                mkPlt(name + "__minMbb_bJets_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMinMbb[0]) + op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMinMbb[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched true b jets): b jet pair with min. M(bb)")
                mkPlt(name + "__minMbb_bJets_DRbb", op.deltaR(extraBJetCoupleMinMbb[0].p4, extraBJetCoupleMinMbb[1].p4), sel, EqBin(40, 0, 3), title="#Delta R(bb): b jet pair with min. M(bb)")
                mkPlt(name + "__minMbb_bJets_Mbb", op.invariant_mass(extraBJetCoupleMinMbb[0].p4, extraBJetCoupleMinMbb[1].p4), sel, EqBin(40, 0, 200), title="M(bb): b jet pair with min. M(bb)")
                mkPlt(name + "__minMbb_bJets_pTbb", (extraBJetCoupleMinMbb[0].p4 + extraBJetCoupleMinMbb[1].p4).Pt(), sel, EqBin(40, 0, 400), title="pT(bb): b jet pair with min. M(bb)")

                # leading b-jets
                mkPlt(name + "__leading_bJets_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == bJets[0]) + op.rng_any(unmatched_bJets_true, lambda j: j == bJets[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched true b jets): leading + subleading b jets")
                mkPlt(name + "__leading_bJets_DRbb", op.deltaR(bJets[0].p4, bJets[1].p4), sel, EqBin(40, 0, 6), title="#Delta R(bb): 1st and 2nd by pt")
                mkPlt(name + "__leading_bJets_Mbb", op.invariant_mass(bJets[0].p4, bJets[1].p4), sel, EqBin(20, 0, 1000), title="M(bb): 1st and 2nd by pt")
                mkPlt(name + "__leading_bJets_pTbb", (bJets[0].p4 + bJets[1].p4).Pt(), sel, EqBin(40, 0, 400), title="pT(bb): 1st and 2nd by pt")

                # leading b-jets
                mkPlt(name + "__csv34_bJets_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == bJetsByDeepFlav[2]) + op.rng_any(unmatched_bJets, lambda j: j == bJetsByDeepFlav[3]),
                         sel, EqBin(3, 0, 3), title="#(unmatched true b jets): 3rd and 4th b jets by deepFlav")

                # average DRbb
                mkPlt(name + "__average_DRbb", op.rng_mean(op.map(bJetCouples, lambda p: op.deltaR(p[0].p4, p[1].p4))), sel, EqBin(40, 0.6, 4), title="Average #Delta R(bb)")

            # 3rd and 4th subleading b-jets
            if name == "5j3b":
                mkPlt(name + "__subleading3_bJet_isUnMatchedTrue", op.rng_any(unmatched_bJets_true, lambda j: j == bJets[2]), sel, EqBin(2, 0, 2), title="3rd b jet is true unmatched b?")
            elif name == "6j4b":
                mkPlt(name + "__subleading34_bJets_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == bJets[2]) + op.rng_any(unmatched_bJets_true, lambda j: j == bJets[3]),
                      sel, EqBin(3, 0, 3), title="#(unmatched true b jets): 3rd and 4th b jet")
                mkPlt(name + "__subleading34_bJets_DRbb", op.deltaR(bJets[2].p4, bJets[3].p4), sel, EqBin(40, 0, 6), title="#Delta R(bb): 3rd and 4th by pt")
                mkPlt(name + "__subleading34_bJets_Mbb", op.invariant_mass(bJets[2].p4, bJets[3].p4), sel, EqBin(20, 0, 300), title="M(bb): 3rd and 4th by pt")
                mkPlt(name + "__subleading34_bJets_pTbb", (bJets[2].p4 + bJets[3].p4).Pt(), sel, EqBin(40, 0, 300), title="pT(bb): 3rd and 4th by pt")

            # for top matching we need at least 2 light jets
            sel = sel.refine(sel.name + "_2light", cut=op.rng_len(lightJets) >= 2)

            mkPlt(name + "__recMatch_chi2", recMatch_chi2, sel, EqBin(40, 0, 40), title="Had. top matching Chi2")

            # W and t true masses - if matching jets exist
            sel_hadTop_eq_flav = sel.refine(sel.name + "_hadTop_flav", cut=recoJets_match_hadTop_flav)
            mkPlt(name + "_match_hadTop_flav__trueMatchedJets_W_mass", op.invariant_mass(wBoson_jet_p4), sel_hadTop_eq_flav, EqBin(60, 30, 130), title="m(W) from correctly matched jets")
            mkPlt(name + "_match_hadTop_flav__trueMatchedJets_t_mass", op.invariant_mass(tQuark_jet_p4), sel_hadTop_eq_flav, EqBin(60, 100, 240), title="m(t) from correctly matched jets")

            # correct chi2 matching after chi2 cut? - if matching jets exist
            sel_hadTop_eq_flav_chi2 = sel_hadTop_eq_flav.refine(sel_hadTop_eq_flav.name + "_chi2", cut=recMatch_chi2Cut)
            mkPlt(name + "_match_hadTop_eq_flav_chi2__recJets_W_correct", wBoson_recJets_correct, sel_hadTop_eq_flav_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. W?")
            mkPlt(name + "_match_hadTop_eq_flav_chi2__recJets_t_correct", tQuark_recJets_correct, sel_hadTop_eq_flav_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. top?")

            # correct chi2 matching after chi2 cut? - overall
            sel = sel.refine(sel.name + "_recMatchChi2", cut=recMatch_chi2Cut)
            mkPlt(name + "_recMatchChi2__recJets_W_correct", op.AND(recoJets_match_hadTop_flav, wBoson_recJets_correct), sel, EqBin(2, 0, 2), title="Correct jets matched to had. W?")
            mkPlt(name + "_recMatchChi2__recJets_t_correct", op.AND(recoJets_match_hadTop_flav, tQuark_recJets_correct), sel, EqBin(2, 0, 2), title="Correct jets matched to had. top?")

            # jets not matched using chi2
            if name == "5j3b":
                mkPlt(name + "_recMatchChi2__leading_bJet_notRec_isUnMatchedTrue", op.rng_any(unmatched_bJets_true, lambda j: j == bJets_notRec[0]), sel, EqBin(2, 0, 2), title="Leading remaining b jet is unmatched true?")
                mkPlt(name + "_recMatchChi2__subleading_bJet_notRec_isUnMatchedTrue", op.rng_any(unmatched_bJets_true, lambda j: j == bJets_notRec[1]), sel, EqBin(2, 0, 2), title="Subleading remaining b jet is unmatched true?")
            elif name == "6j4b":
                # define combinations of b jets using unmatched jets only
                bJetCouples_notRec = op.combine(bJets_notRec, N=2)
                extraBJetCoupleMinDRbb_notRec = op.rng_min_element_by(bJetCouples_notRec, lambda pair: op.deltaR(pair[0].p4, pair[1].p4))
                extraBJetCoupleMinMbb_notRec = op.rng_min_element_by(bJetCouples_notRec, lambda pair: op.invariant_mass_squared(pair[0].p4, pair[1].p4))
                extraBJetCoupleMaxMbb_notRec = op.rng_max_element_by(bJetCouples_notRec, lambda pair: op.invariant_mass_squared(pair[0].p4, pair[1].p4))
                maxBBnotRecSubLeadingGenJet = extraBJetCoupleMaxMbb_notRec[1]
                maxBBRemainingNotRecLeadingGenJet = op.select(bJets_notRec, lambda j: op.AND(j != extraBJetCoupleMaxMbb_notRec[0], j != extraBJetCoupleMaxMbb_notRec[1]))[0]

                mkPlt(name + "_recMatchChi2__leading_bJets_notRec_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == bJets_notRec[0]) + op.rng_any(unmatched_bJets_true, lambda j: j == bJets_notRec[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched true b jets) in remaining, 1st and 2nd")
                mkPlt(name + "_recMatchChi2__subleading23_bJets_notRec_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == bJets_notRec[1]) + op.rng_any(unmatched_bJets_true, lambda j: j == bJets_notRec[2]),
                         sel, EqBin(3, 0, 3), title="#(unmatched true b jets) in remaining, 2nd and 3rd")
                mkPlt(name + "_recMatchChi2__minDRbb_bJets_notRec_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMinDRbb_notRec[0]) + op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMinDRbb_notRec[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched true b jets) in remaining, min #Delta R(bb) pair")
                mkPlt(name + "_recMatchChi2__maxMbb_bJets_notRec_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMaxMbb_notRec[0]) + op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMaxMbb_notRec[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched true b jets) in remaining, max M(bb) pair")
                mkPlt(name + "_recMatchChi2__minMbb_bJets_notRec_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMinMbb_notRec[0]) + op.rng_any(unmatched_bJets_true, lambda j: j == extraBJetCoupleMinMbb_notRec[1]),
                         sel, EqBin(3, 0, 3), title="#(unmatched true b jets) in remaining, min M(bb) pair")
                mkPlt(name + "_recMatchChi2__maxMbbTwist_bJets_notRec_areUnMatchedTrue",
                         op.rng_any(unmatched_bJets_true, lambda j: j == maxBBnotRecSubLeadingGenJet) + op.rng_any(unmatched_bJets_true, lambda j: j == maxBBRemainingNotRecLeadingGenJet),
                         sel, EqBin(3, 0, 3), title="#(unmatched true b jets) in remaining (**), max M(bb) pair")

        for name,sel in [("6j3b1l", oneLep6Jet3B1LSel), ("7j4b1l", oneLep7Jet4B1LSel)]:
            plots.append(SummedPlot(name + "_l_from_t_pt", [
                Plot.make1D(name + "_l_from_t_0_pt", matchedJets["l0"].pt, mkSel(sel, op.AND(nMatchJets["l0"] > 0, isTagL(matchedJets["l0"]))), EqBin(40, 30, 300)),
                Plot.make1D(name + "_l_from_t_1_pt", matchedJets["l1"].pt, mkSel(sel, op.AND(nMatchJets["l1"] > 0, isTagL(matchedJets["l1"]))), EqBin(40, 30, 300))
            ], title="Pt of light jets matched to light quarks from top"))

            mkPlt(name + "__nUnmatchedLightJets", op.rng_len(unmatched_lightJets), sel, EqBin(10, 0, 10), title="# unmatched jets")

            # have at least one unmatched light jet
            sel_1lightNotMatched = sel.refine(sel.name + "_unMatched", cut=op.rng_len(unmatched_lightJets_true) > 0)
            mkPlt(name + "__leading_unmatched_lightJet_pt", unmatched_lightJets_true[0].pt, sel_1lightNotMatched, EqBin(40, 30, 300), title="pt of leading unmatched true light jet")

            mkPlt(name + "__unmatched_lightJets_pt", op.map(unmatched_lightJets_true, lambda j: j.pt), sel, EqBin(40, 30, 300), title="pt of unmatched true light jets")
            mkPlt(name + "__leading_lightJet_isUnMatchedTrue", op.rng_any(unmatched_lightJets_true, lambda j: j == lightJets[0]), sel, EqBin(2, 0, 2), title="Leading true light jet is unmatched?")

            sel = sel.refine(sel.name + "_2light", cut=op.rng_len(lightJets) >= 2)

            # top rec matching

            sel_chi2 = mkSel(sel, recMatch_chi2Cut)
            mkPlt(name + "_recMatchChi2__recJets_W_correct", op.AND(recoJets_match_hadTop_flav, wBoson_recJets_correct), sel_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. W?")
            mkPlt(name + "_recMatchChi2__recJets_t_correct", op.AND(recoJets_match_hadTop_flav, tQuark_recJets_correct), sel_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. top?")
            sel_hadTop_eq_flav_chi2 = mkSel(sel_chi2, recoJets_match_hadTop_flav)
            mkPlt(name + "_match_hadTop_eq_flav_chi2__recJets_W_correct", wBoson_recJets_correct, sel_hadTop_eq_flav_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. W?")
            mkPlt(name + "_match_hadTop_eq_flav_chi2__recJets_t_correct", tQuark_recJets_correct, sel_hadTop_eq_flav_chi2, EqBin(2, 0, 2), title="Correct jets matched to had. top?")

            sel = mkSel(sel, op.rng_len(lightJets) >= 3)
            mkPlt(name + "__3rd_lightJet_pt", lightJets[2].pt, sel, EqBin(40, 30, 200), title="pt of 3rd light jet")
            mkPlt(name + "__3rd_lightJet_isUnMatchedTrue", op.rng_any(unmatched_lightJets_true, lambda j: j == lightJets[2]), sel, EqBin(2, 0, 2), title="3rd light jet is true unmatched?")

            mkPlt(name + "__matchWOnly_leading_lightJet_isUnMatchedTrue", op.rng_any(unmatched_lightJets_true, lambda j: op.switch(wMatchingChi2(jets_LL_wOnly_best_p4) < 4., j == lightJets_wOnly_notRec[0], j == lightJets[0])), sel, EqBin(2, 0, 2), title="Leading remaining light jet is true unmatched?")

            recSel = mkSel(sel, op.AND(recMatch_chi2Cut, op.rng_len(lightJets_notRec) > 0))
            mkPlt(name + "_recMatchChi2__leading_lightJet_isUnMatchedTrue", op.rng_any(unmatched_lightJets_true, lambda j: j == lightJets_notRec[0]), recSel, EqBin(2, 0, 2), title="Leading remaining light jet is true unmatched?")

        return plots

    def postProcess(self, taskList, config=None, workdir=None, resultsdir=None):
        config["plotIt"]["legend"]["position"] = [0.5, 0.85, 0.95, 0.91]

        # finally, run plotIt as defined in HistogramsModule
        # also remove the file lists from the yml config:
        for smpName, smpCfg in config["samples"].items():
            smpCfg.pop("files")
            smpCfg["line-width"] = 2
            if "TTbb" in smpName:
                smpCfg["legend"] = "TTbb 4FS l+jets"
                smpCfg["line-color"] = "#912200"
            else:
                smpCfg["legend"] = "TT inclusive 5FS l+jets (ttB)"
                smpCfg["line-color"] = "#F09175"
        super(genTtbbPlotter, self).postProcess(taskList, config, workdir, resultsdir)
