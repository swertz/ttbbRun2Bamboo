#! /bin/env python

import os, sys, argparse
import yaml
from math import floor

# to prevent pyroot to hijack argparse we need to go around
tmpargv = sys.argv[:] 
sys.argv = []

# ROOT imports
import ROOT
ROOT.gROOT.SetBatch()
ROOT.PyConfig.IgnoreCommandLineOptions = True
sys.argv = tmpargv

import HistogramTools as HT

# colors = ['#468966', '#8E2800']
colors = [
    "#7044f1",
    "#45ad95",
    "#c07f00",
    "#da7fb7",
    "#ff2719",
    "#251c00"]

def halfRound(r):
    """Round up so that the decimal part has only one digit:
        0.045 -> 0.05
        0.0091 -> 0.01
        0.01 -> 0.01
        1.4 -> 2.
    """
    if r == 0:
        return r
    if r < 0:
        return -halfRound(-r)
    power = 0
    while r < 1:
        r *= 10
        power -= 1
    return (1 + floor(r)) * pow(10., power)

def drawPlot(name, plot, outDir):
    doRatio = plot.get("ratio", True)
    hi_scaling = .6666 if doRatio else 1
    logx = plot.get("logx", False)
    logy = plot.get("logy", False)
    norm = plot.get("norm", False)

    c = ROOT.TCanvas(HT.randomString(), "c", 800, 800)

    hi_pad = ROOT.TPad(HT.randomString(), "", 0., 0.33333 if doRatio else 0, 1, 1)
    hi_pad.Draw()
    hi_pad.SetTopMargin(0.06 / hi_scaling)
    hi_pad.SetLeftMargin(0.16)
    if doRatio:
        hi_pad.SetBottomMargin(0.015)
    else:
        hi_pad.SetBottomMargin(0.13)
    hi_pad.SetRightMargin(0.05)
    hi_pad.SetTickx(1)
    hi_pad.SetTicky(1)

    if doRatio:
        lo_pad = ROOT.TPad(HT.randomString(), "", 0., 0., 1, 0.33333)
        lo_pad.Draw()
        lo_pad.SetTopMargin(1)
        lo_pad.SetLeftMargin(0.16)
        lo_pad.SetBottomMargin(0.13 / 0.33333)
        lo_pad.SetRightMargin(0.05)
        lo_pad.SetTickx(1)
        lo_pad.SetTicky(1)

    hi_pad.cd()

    if doRatio:
        nomVar = [ v for v in plot["vars"] if v.get("nominal", False) ][0]
        variations = [ v for v in plot["vars"] if v != nomVar ]
    else:
        nomVar = plot["vars"][0]
        variations = plot["vars"][1:]
    nominal = nomVar["hist"]

    nominal.SetLineWidth(2)
    if doRatio:
        nominal.SetLineStyle(2)
    nominal.SetLineColor(ROOT.TColor.GetColor(plot.get("color", '#FFB03B' if doRatio else colors[0])))
    nominal.GetYaxis().SetLabelSize(0.02 / hi_scaling)
    nominal.GetYaxis().SetTitleSize(0.03 / hi_scaling)
    nominal.GetYaxis().SetTitleOffset(1.7 * hi_scaling)
    nominal.GetYaxis().SetTitle(plot.get("yTitle", "Events"))
    if doRatio:
        nominal.GetXaxis().SetLabelSize(0)
    else:
        nominal.GetXaxis().SetLabelSize(0.02)
        nominal.GetXaxis().SetTitleSize(0.03)
        nominal.GetXaxis().SetTitleOffset(1.5)
        nominal.GetXaxis().SetTitle(plot.get("xTitle", ""))
    if norm:
        nominal.Scale(1./nominal.Integral())
    nominal.Draw("hist")

    for i,v in enumerate(variations):
        h = v["hist"]
        if norm:
            h.Scale(1./h.Integral())
        h.SetLineWidth(2)
        h.SetLineColor(ROOT.TColor.GetColor(v.get("color", colors[i if doRatio else i+1])))
        h.SetMarkerColor(ROOT.TColor.GetColor(v.get("color", colors[i if doRatio else i+1])))
        h.Draw("hist same")

    hist_max = float("-inf")
    hist_min = float("inf")
    for i in range(1, nominal.GetNbinsX() + 1):
        contents = [v["hist"].GetBinContent(i) for v in variations + [nomVar]]
        contents = [ c for c in contents if c > 0 ]
        if len(contents):
            hist_max = max(hist_max, *contents)
            hist_min = min(hist_min, *contents)
    if logy:
        nominal.GetYaxis().SetRangeUser(hist_min * 0.9, hist_max * 1.4)
        hi_pad.SetLogy()
    else:
        nominal.GetYaxis().SetRangeUser(0, hist_max * 1.25)
    if logx:
        hi_pad.SetLogx()

    if doRatio:
        lo_pad.cd()
        lo_pad.SetGrid()

        if logx:
            lo_pad.SetLogx()

        ratios = [ v["hist"].Clone(HT.randomString()) for v in variations ]
        for r in ratios:
            # r.SetDirectory(0)
            r.Divide(nominal)

        ratio_style = "hist"

        ratios[0].GetXaxis().SetLabelSize(0.02 / 0.333)
        ratios[0].GetXaxis().SetTitleSize(0.03 / 0.333)
        ratios[0].GetXaxis().SetLabelOffset(0.05)
        ratios[0].GetXaxis().SetTitleOffset(1.5)
        ratios[0].GetXaxis().SetTitle(plot["xTitle"])
        ratios[0].GetYaxis().SetLabelSize(0.02 / 0.333)
        ratios[0].GetYaxis().SetTitleSize(0.03 / 0.333)
        ratios[0].GetYaxis().SetTitleOffset(1.7 * 0.333)
        ratios[0].GetYaxis().SetTitle("")
        ratios[0].GetYaxis().SetNdivisions(502, True)
        ratios[0].GetYaxis().SetRangeUser(0.5, 1.5)

        ratios[0].SetMarkerStyle(20)
        ratios[0].SetMarkerSize(0.6)
        ratios[0].Draw(ratio_style)

        line = ROOT.TLine(ratios[0].GetXaxis().GetBinLowEdge(1), 1, ratios[0].GetXaxis().GetBinUpEdge(ratios[0].GetXaxis().GetLast()), 1)
        line.Draw("same")

        ratios[0].Draw(ratio_style + "same")

        for i, ratio in enumerate(ratios[1:]):
            ratio.SetMarkerStyle(20)
            ratio.SetMarkerSize(0.6)
            ratio.Draw(ratio_style + "same")

        # Look for min and max of ratio and zoom accordingly
        ratio_max = 1.
        ratio_min = 1.
        for i in range(1, ratios[0].GetNbinsX() + 1):
            contents = [r.GetBinContent(i) for r in ratios if r.GetBinContent(i) > 0]
            if len(contents):
                ratio_max = max(ratio_max, *contents)
                ratio_min = min(ratio_min, *contents)

        # Symetrize
        ratio_range = halfRound(max(abs(ratio_max - 1), abs(1 - ratio_min)))
        ratios[0].GetYaxis().SetRangeUser(max(0, 1 - ratio_range), 1 + ratio_range)
        # ratios[0].GetYaxis().SetRangeUser(1 - halfRound(1 - ratio_min), 1 + halfRound(ratio_max - 1))
        # ratios[0].GetYaxis().SetRangeUser(0.999 * ratio_min, 1.001 * ratio_max)
        ratios[0].GetYaxis().SetNdivisions(210)
        ratios[0].GetYaxis().SetTitle('Ratio')

    c.cd()

    if any("leg" in v for v in variations + [nomVar]):
        l = ROOT.TLegend(*plot.get("leg-pos", [0.58, 0.79, 0.92, 0.92]))
        l.SetTextFont(42)
        l.SetFillColor(ROOT.kWhite)
        l.SetFillStyle(0)
        l.SetBorderSize(0)

        l.AddEntry(nominal, nomVar["leg"])
        for v in variations:
            l.AddEntry(v["hist"], v["leg"])
        l.Draw("same")

    if "title" in plot:
        text = plot["title"]
        syst_text = ROOT.TLatex(0.16, 0.97, text)
        syst_text.SetNDC(True)
        syst_text.SetTextFont(42)
        syst_text.SetTextSize(0.03)
        syst_text.Draw("same")

    ROOT.gErrorIgnoreLevel = ROOT.kWarning
    for form in plot.get("format", ["pdf"]):
        c.SaveAs(os.path.join(outDir, name + "." + form))

    # pyROOT bug - see https://root-forum.cern.ch/t/pyroot-canvas-with-pads-crash/34726
    # hi_pad.Delete()
    # if doRatio:
    #     lo_pad.Delete()

def drawHisto(path, cache, variable, cut, binning=""):
    tf = cache.get(path)
    tt = tf.Get("Events")
    name = HT.randomString()
    tt.Draw(variable + ">>" + name + binning, cut)
    hist = ROOT.gDirectory.Get(name)
    hist.SetDirectory(0)
    return hist

def plotFromConfig(plotCfg, outDir, cache):
    HT.setTDRStyle()

    for name, plt in plotCfg.items():
        if "hist" in plt or any("hist" in var for var in plt["vars"]):
            for var in plt["vars"]:
                path = os.path.join(plt.get("root", ""), var["file"] if "file" in var else plt["file"])
                var["hist"] = HT.loadHisto(path, var["hist"] if "hist" in var else plt["hist"], cache)
            drawPlot(name, plt, outDir)
        else:
            def plotVar(var, binning=""):
                cutStr = "(" + plt.get("cut", "1.") + ")*(" + plt.get("weight", "1.") + ")*(" + var.get("weight", "1.") + ")"
                varStr = var["variable"] if "variable" in var else plt["variable"]
                path = os.path.join(plt.get("root", ""), var["file"] if "file" in var else plt["file"])
                var["hist"] = drawHisto(path, cache, varStr, cutStr, binning)
            if "bin" in plt:
                for var in plt["vars"]:
                    plotVar(var, plt["bin"])
            else:
                plotVar(plt["vars"][0])
                binHist = plt["vars"][0]["hist"]
                binning = f"({binHist.GetXaxis().GetNbins()},{binHist.GetXaxis().GetXmin()},{binHist.GetXaxis().GetXmax()})"
                for var in plt["vars"][1:]:
                    plotVar(var, binning)
            drawPlot(name, plt, outDir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Produce quickly not too ugly simple plots')
    parser.add_argument('-o', '--output', default='./', help='Output directory')
    parser.add_argument('-c', '--config', help='YML config')

    options = parser.parse_args()

    os.makedirs(options.output, exist_ok=True)

    with open(options.config) as f:
        plotCfg = yaml.load(f)

    plotFromConfig(plotCfg, options.output, HT.FileCache())
